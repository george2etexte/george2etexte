<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="dist/css/bootstrap.css">
    <link rel="stylesheet" href="dist/css/normalize.css">
    <title>Accueil</title>
</head>
<body>
   <a href="profil_deco.php">Se déconnecter</a>
    <?php
    function chargerClasse($classe){
        require "test/Class/".$classe.".php";
    }
    spl_autoload_register('chargerClasse');
    session_start();
    include('parameters.php');
    
    $manager = new UtilisatriceManager($bdd);
    $utilisatrice = $manager->getUtilisatriceById($_SESSION['id']);
    echo $utilisatrice->affiche();
    ?>
    <a href="profil_prive.php">Infos privées</a>
</body>
</html>