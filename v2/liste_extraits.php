<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Liste complète des extraits</title>
    <link rel="stylesheet" href="./style.css" />
   <link href="./dist/css/bootstrap.css" rel="stylesheet">
   <link rel="shortcut icon" href="./images/icone_george2etexte.ico" type="images/x-icon" /> 
</head>
<body style="background-color:white;font-size:12pt;">
    <SCRIPT TYPE="text/javascript" SRC="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></SCRIPT>
<SCRIPT>
$(document).ready(function(){
   $("h2").hide();
})
</SCRIPT>

<?php
    include('header.php');
    include('parameters.php');
    include('functions.php');
    
    function chargerClasse($classe){
        require 'test/Class/'.$classe.'.php';
    }
    spl_autoload_register('chargerClasse');
    
    $managerU = new UtilisatriceManager($bdd);
    $managerE = new ExtraitManager($bdd);
    $managerT = new TagManager($bdd);
    $managerN = new NotionManager($bdd);
    $managerP = new PeriodeManager($bdd);
    
    ?>
    <div style="background-color:#F5F5F5;margin-top:20px;padding:20px;">
        <div class="container">
            <div class="panel panel-default" style="text-align:center;padding:20px;">
                <div class="panel-body">
                  <h1 class="form-signing-heading">Liste de tous les extraits</h1>
                  <?php
                    if($_GET == array()){
                        ?>
    <form action="recherche_extraits.php" method="get">
          <fieldset><legend>Recherche par mot: </legend>
              <input type="search" name="donneescherche" id="donneescherche" class="form-control" required placeholder="Sujet de la recherche"><br><br>
              
              <input type="image" src="images/magnifier.svg" width="80" height="80" value="Validez">
          </fieldset>
      </form>
      <hr>
      <form action="recherche_extraits_alternative.php" method="get">
          <fieldset><legend>Recherche experte: </legend>
        <span>Je cherche un texte <select name="periode" id="periode">
                  <option value="">-- Si vous le souhaitez, choisissez une période --</option>
                  <?php
          $managerP->getAllPeriodesDet();
                  ?>
              </select> dans le genre littéraire <select name="genre" id="genre">
                  <option value="">-- Si vous le souhaitez, choisissez un genre littéraire --</option>
                  <?php
          $managerT->getAllTagsByType(2);
            ?>
              </select>, en rapport avec la notion <select name="notion" id="notion">
                  <option value="">-- Si vous le souhaitez, choisissez une notion --</option>
                  <?php
          $managerN->getAllNotionsDet();
                  ?>
            </select> et la thématique <select name="tag" id="tag">
                  <option value="">-- Si vous le souhaitez, choisissez une thématique --</option>
                  <?php
          $managerT->getAllTagsByType(0);
                  ?>
              </select>
          </span><br><br>
              
              <input type="image" src="images/magnifier.svg" width="80" height="80" value="Validez">
          </fieldset>
              
      </form>
                 <?php
                    }
                    ?>
                  <hr>
                   <?php
                    $utilisatrice = 0;
                    
                    $managerE->allTexts($utilisatrice, 0, 0, 0, array());
                    ?>
                    <a href="extraits.php">Retour</a>
                </div>
            </div>
        </div>
    </div>
    
    <?php
    include('footer.php');
    ?>

</body>
</html>