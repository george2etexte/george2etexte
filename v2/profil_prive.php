<?php
session_start();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="dist/css/bootstrap.css">
    <link rel="stylesheet" href="dist/css/normalize.css">
    <link rel="stylesheet" href="style.css">
    <title>Profil privé</title>
</head>
<body>
    <?php
    function chargerClasse($classe){
        require "test/Class/".$classe.".php";
    }
    spl_autoload_register('chargerClasse');
    include('parameters.php');
    
    $manager = new UtilisatriceManager($bdd);
    $utilisatrice = $manager->getUtilisatriceById($_SESSION['id']);
    
    echo $utilisatrice->afficheProfil();
    ?>
    <a href="profil_modif.php">Modifier le profil</a><br>
    <a href="index.php">Retour</a>
</body>
</html>