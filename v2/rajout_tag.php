<?php

include('parameters.php');
include('functions.php');

function chargerClasse($classe){
    require 'test/Class/'.$classe.'.php';
}
spl_autoload_register('chargerClasse');

$manager = new TagManager($bdd);

$id_extrait = intval($_POST['idextrait']);

if(isset($_POST['idtags']) && !empty($_POST['idtags'])){
    $idtags = explode(",", $_POST['idtags']);
    
    for($i = 0; $i < sizeof($idtags); $i++){
        $idtags[$i] = intval($idtags[$i]);
    }
    
} else {
    $idtags = array();
}

$crea_tags = array();

if(isset($_POST['theme_crea']) && !empty($_POST['theme_crea'])){
    $themes = explode(",", $_POST['theme_crea']);
    
    for($j = 0; $j < sizeof($themes); $j++){
        $themes[$j] = $manager->formatTag($themes[$j]);
        $index = sizeof($crea_tags);
        $crea_tags[$index] = new Tag(array('nom_tag' => $themes[$j], 'type_tag' => 0));
    }
}

if(isset($_POST['periode_crea']) && !empty($_POST['periode_crea'])){
    $periodes = explode(",", $_POST['periode_crea']);
    
    for($k = 0; $k < sizeof($periodes); $k++){
        $periodes[$k] = $manager->formatTag($periodes[$k]);
        $index2 = sizeof($crea_tags);
        $crea_tags[$index2] = new Tag(array('nom_tag' => $periodes[$k], 'type_tag' => 1));
    }
}

if(isset($_POST['genre_crea']) && !empty($_POST['genre_crea'])){
    $genres = explode(",", $_POST['genre_crea']);
    
    for($l = 0; $l < sizeof($genres); $l++){
        $genres[$l] = $manager->formatTag($genres[$l]);
        $index3 = sizeof($crea_tags);
        $crea_tags[$index3] = new Tag(array('nom_tag' => $genres[$l], 'type_tag' => 2));
    }
}

if(isset($_POST['notion_crea']) && !empty($_POST['notion_crea'])){
    $notions = explode(",", $_POST['notion_crea']);
    
    for($m = 0; $m < sizeof($notions); $m++){
        $notions[$m] = $manager->formatTag($notions[$m]);
        $index4 = sizeof($crea_tags);
        $crea_tags[$index4] = new Tag(array('nom_tag' => $notions[$m], 'type_tag' => 3));
    }
}

$manager->insertTagsInExtrait($idtags, $id_extrait);

if($crea_tags !== array()){
    $manager->insertNewTagInExtrait($crea_tags, $id_extrait);
}

?>