// Define last author seen as a global variable, used in the addWriter function to avoid duplicates
var dernierAuteur="";

// Add a writer to the result list, using its information retrieved from WikiData
function addWriter(data){
   //console.log(data);
   // If the author name was not already seen
   if(data.writer.value!=dernierAuteur){
      dernierAuteur=data.writer.value;
      var chaine="";
      chaine=chaine+"<a href=\"http://data.bnf.fr/ark:/12148/cb"+(data.bnfId.value)+"\">"+(data.writer.value)+"</a>";
      chaine=chaine+" <a href=\"http://data.bnf.fr/ark:/12148/cb"+(data.bnfId.value)+"\"><img src=\"./images/icone_data_bnf.ico\" alt=\""+(data.writer.value)+" sur Data BNF\"></a>";
      chaine=chaine+" <a href=\"http://gallica.bnf.fr/services/engine/search/sru?operation=searchRetrieve&amp;exactSearch=false&amp;collapsing=true&amp;version=1.2&amp;query=(dc.creator%20all%20%22"+(data.writer.value)+"%22%20or%20dc.contributor%20all%20%22"+(data.writer.value)+"%22%20)%20&amp;suggest=10\"><img src=\"./images/icone_gallica.ico\" alt=\""+(data.writer.value)+" sur Gallica\"></a>";
      chaine=chaine+" <a href=\"https://www.qwant.com/?q="+(data.writer.value)+"&amp;t=all\"><img src=\"./images/icone_qwant.ico\" alt=\""+(data.writer.value)+" sur Qwant\"></a>";
      if(parseInt(data.yob.value)<1769){
         chaine=chaine+" <a href=\"http://siefar.org/mediawiki/fr/index.php?title=Spécial%3ARecherche&amp;profile=default&amp;search="+(data.writer.value)+"&amp;fulltext=Search\"><img src=\"./images/icone_siefar.ico\" alt=\""+(data.writer.value)+" dans les notices de la SIEFAR\"></a>";
      }
      chaine=chaine+"<br/>";
      $("#resultatsAlterEgo").append(chaine);
   }
}

// Check if a result was found for the query on the author name
function getBirthDeathDateResults(data){
   if(data.results.bindings.length==0){
      $("#resultatsAlterEgo").html("0 résultat trouvé : avez-vous bien entré le prénom suivi du nom, commençant tous deux par des majuscules ?");
   } else {
      getBirthDeathDates(data);
   }
}

// Treat results obtained about the birth and death date of an author
// Launch a WikiData query to obtain all women authors in the same period
function getBirthDeathDates(data){
      console.log(data);
      var birthDate=data.results.bindings[0].dob.value;
      var deathDate;
      if (data.results.bindings[0].dod!=undefined){
         deathDate=data.results.bindings[0].dod.value;
      } else {
         deathDate="2019-01-01";     
      }
      var bYear=birthDate.substring(0,4);
      var dYear=deathDate.substring(0,4);
      console.log(bYear+"|"+dYear);
      if(isNaN(dYear)){
          dYear=bYear+80;
      }
      if(isNaN(bYear)){
          bYear=dYear-80;
      }
      $("#resultatsAlterEgo").html("<br/>Autrices nées avant "+(parseInt(dYear)-15)+", et décédées après "+(parseInt(bYear)+15)+" ou nées après "+(parseInt(bYear)-15)+" selon WikiData (la recherche peut prendre plusieurs secondes...) :<br/>");
      //Previous version of the query:
      //var query = "https://query.wikidata.org/bigdata/namespace/wdq/sparql?query=%23Autrices%20en%20fonction%20de%20dates%20de%20naissance%20%2F%20d%C3%A9c%C3%A8s%0ASELECT%20distinct%20%3Fwriter%20%3Fplace%20%3FbnfId%20%3Fyob%20%3Fyod%20%28COUNT%28DISTINCT%20%3Fsitelink%29%20as%20%3Flinkcount%29%0AWHERE%0A%7B%0A%20%20%23%20%C3%A9crivaine%2C%20autrice%2C%20po%C3%A9tesse%0A%20%20%7B%3Fs%20wdt%3AP106%20wd%3AQ36180%20.%7D%20UNION%20%7B%20%3Fs%20wdt%3AP106%20wd%3AQ482980%20.%20%7D%20UNION%20%7B%20%3Fs%20wdt%3AP106%20wd%3AQ49757%20.%20%7D%0A%20%20%23%20nationalit%C3%A9%0A%20%20%3Fs%20wdt%3AP27%20%3Fpl%20.%0A%20%20%3Fs%20wdt%3AP268%20%3FbnfId%20.%0A%20%20%23%20langue%20fran%C3%A7aise%0A%20%20%7B%3Fs%20wdt%3AP37%7Cwdt%3AP103%7Cwdt%3AP364%7Cwdt%3AP407%7Cwdt%3AP1412%7Cwdt%3AP2439%20wd%3AQ150%20.%7D%20UNION%20%7B%3Fs%20wdt%3AP37%7Cwdt%3AP103%7Cwdt%3AP364%7Cwdt%3AP407%7Cwdt%3AP1412%7Cwdt%3AP2439%20wd%3AQ1473289%20.%7D%0A%20%20%23%20genre%0A%20%20%7B%3Fs%20wdt%3AP21%20wd%3AQ6581072%20.%7D%20UNION%20%7B%3Fs%20wdt%3AP21%20wd%3AQ1097630%20.%7D%20UNION%20%7B%3Fs%20wdt%3AP21%20wd%3AQ1052281%20.%7D%20UNION%20%7B%3Fs%20wdt%3AP21%20wd%3AQ2449503%20.%7D%0A%20%20%23%20ann%C3%A9e%20de%20naissance%0A%20%20OPTIONAL%20%7B%20%3Fs%20wdt%3AP569%20%3Fdob.%7D%0A%20%20BIND%28YEAR%28%3Fdob%29%20as%20%3Fyob%29%20.%0A%20%20%23%20ann%C3%A9e%20de%20d%C3%A9c%C3%A8s%0A%20%20OPTIONAL%20%7B%20%3Fs%20wdt%3AP570%20%3Fdod.%7D%0A%20%20BIND%28YEAR%28%3Fdod%29%20as%20%3Fyod%29%20.%0A%20%20%3Fsitelink%20schema%3Aabout%20%3Fs%20.%0A%20%20OPTIONAL%20%7B%0A%20%20%20%20%20%3Fs%20rdfs%3Alabel%20%3Fwriter%20filter%20%28lang%28%3Fwriter%29%20%3D%20%22fr%22%29.%0A%20%20%7D%0A%20%20OPTIONAL%20%7B%0A%20%20%20%20%20%3Fpl%20rdfs%3Alabel%20%3Fplace%20filter%20%28lang%28%3Fplace%29%20%3D%20%22fr%22%29.%0A%20%20%7D%0A%20%20FILTER%28%28%3Fyob%3C%3D"+(parseInt(dYear)-15)+"%29%20%26%26%20%28%3Fyod%3E%3D"+(parseInt(bYear)+15)+"%29%29%0A%7D%20GROUP%20BY%20%3Fplace%20%3Fwriter%20%3FbnfId%20%3Fyob%20%3Fyod%20ORDER%20BY%20DESC%28%3Flinkcount%29&format=json";
      //version en place jusqu'au 02/03/2018 : var query = "https://query.wikidata.org/bigdata/namespace/wdq/sparql?query=%23Autrices%20en%20fonction%20de%20dates%20de%20naissance%20%2F%20décès%0ASELECT%20distinct%20%3Fwriter%20%3Fplace%20%3FbnfId%20%3Fyob%20%3Fyod%20%28COUNT%28DISTINCT%20%3Fsitelink%29%20as%20%3Flinkcount%29%0AWHERE%0A%7B%0A%20%20%23%20écrivaine%2C%20autrice%2C%20poétesse%0A%20%20%7B%3Fs%20wdt%3AP106%20wd%3AQ36180%20.%7D%20UNION%20%7B%20%3Fs%20wdt%3AP106%20wd%3AQ482980%20.%20%7D%20UNION%20%7B%20%3Fs%20wdt%3AP106%20wd%3AQ49757%20.%20%7D%0A%20%20%23%20nationalité%0A%20%20%3Fs%20wdt%3AP27%20%3Fpl%20.%0A%20%20%3Fs%20wdt%3AP268%20%3FbnfId%20.%0A%20%20%23%20langue%20française%0A%20%20%7B%3Fs%20wdt%3AP1412%20wd%3AQ150%20.%7D%20UNION%20%7B%3Fs%20wdt%3AP1412%20wd%3AQ1473289%20.%7D%0A%20%20%23%20genre%0A%20%20%7B%3Fs%20wdt%3AP21%20wd%3AQ6581072%20.%7D%20UNION%20%7B%3Fs%20wdt%3AP21%20wd%3AQ1097630%20.%7D%20UNION%20%7B%3Fs%20wdt%3AP21%20wd%3AQ1052281%20.%7D%20UNION%20%7B%3Fs%20wdt%3AP21%20wd%3AQ2449503%20.%7D%0A%20%20%23%20année%20de%20naissance%0A%20%20OPTIONAL%20%7B%20%3Fs%20wdt%3AP569%20%3Fdob.%7D%0A%20%20BIND%28YEAR%28%3Fdob%29%20as%20%3Fyob%29%20.%0A%20%20%23%20année%20de%20décès%0A%20%20OPTIONAL%20%7B%20%3Fs%20wdt%3AP570%20%3Fdod.%7D%0A%20%20BIND%28YEAR%28%3Fdod%29%20as%20%3Fyod%29%20.%0A%20%20%3Fsitelink%20schema%3Aabout%20%3Fs%20.%0A%20%20OPTIONAL%20%7B%0A%20%20%20%20%20%3Fs%20rdfs%3Alabel%20%3Fwriter%20filter%20%28lang%28%3Fwriter%29%20%3D%20%22fr%22%29.%0A%20%20%7D%0A%20%20OPTIONAL%20%7B%0A%20%20%20%20%20%3Fpl%20rdfs%3Alabel%20%3Fplace%20filter%20%28lang%28%3Fplace%29%20%3D%20%22fr%22%29.%0A%20%20%7D%0A%20%20FILTER%28%28%3Fyob<%3D"+(parseInt(dYear)-15)+"%29%20%26%26%20%28%3Fyod%3E%3D"+(parseInt(bYear)+15)+"%29%29%0A%7D%20GROUP%20BY%20%3Fplace%20%3Fwriter%20%3FbnfId%20%3Fyob%20%3Fyod%20ORDER%20BY%20DESC%28%3Flinkcount%29&format=json";
      /*
      // Test and improve for the next version?
      #Autrices en fonction de dates de naissance / décès
SELECT distinct ?writer ?bnfId ?yob ?yod (COUNT(DISTINCT ?sitelink) as ?linkcount) (COUNT(DISTINCT ?prof) as ?profcount) (COUNT(DISTINCT ?book) as ?bookcount)
WHERE
{
  # écrivaine, autrice, poétesse
  {?s wdt:P106 wd:Q36180 .} UNION { ?s wdt:P106 wd:Q482980 . } UNION { ?s wdt:P106 wd:Q49757 . }
  ?s wdt:P268 ?bnfId .
  # langue française
  {?s wdt:P1412 wd:Q150 .} UNION {?s wdt:P1412 wd:Q1473289 .}
  # genre
  {?s wdt:P21 wd:Q6581072 .} UNION {?s wdt:P21 wd:Q1097630 .} UNION {?s wdt:P21 wd:Q1052281 .} UNION {?s wdt:P21 wd:Q2449503 .}
  # année de naissance
  OPTIONAL { ?s wdt:P569 ?dob.}
  BIND(YEAR(?dob) as ?yob) .
  # année de décès
  OPTIONAL { ?s wdt:P570 ?dod.}
  BIND(YEAR(?dod) as ?yod) .
  ?sitelink schema:about ?s .
  ?s wdt:P106 ?prof .
  OPTIONAL {?s wdt:P800 ?book .}
  OPTIONAL {
     ?s rdfs:label ?writer filter (lang(?writer) = "fr").
  }
  FILTER(((?yob<=2004) && (?yod>=1971)) || ((?yob<=2004) && (?yob>=1941)) )
} GROUP BY ?writer ?bnfId ?yob ?yod ORDER BY DESC(?linkcount-?profcount*9+?bookcount*?bookcount)
      */
      var query = "https://query.wikidata.org/bigdata/namespace/wdq/sparql?query=%23Autrices%20en%20fonction%20de%20dates%20de%20naissance%20%2F%20d%C3%A9c%C3%A8s%0D%0ASELECT%20distinct%20%3Fwriter%20%3Fplace%20%3FbnfId%20%3Fyob%20%3Fyod%20%28COUNT%28DISTINCT%20%3Fsitelink%29%20as%20%3Flinkcount%29%0D%0AWHERE%0D%0A%7B%0D%0A%20%20%23%20%C3%A9crivaine%2C%20autrice%2C%20po%C3%A9tesse%0D%0A%20%20%7B%3Fs%20wdt%3AP106%20wd%3AQ36180%20.%7D%20UNION%20%7B%20%3Fs%20wdt%3AP106%20wd%3AQ482980%20.%20%7D%20UNION%20%7B%20%3Fs%20wdt%3AP106%20wd%3AQ49757%20.%20%7D%0D%0A%20%20%23%20nationalit%C3%A9%0D%0A%20%20%3Fs%20wdt%3AP27%20%3Fpl%20.%0D%0A%20%20%3Fs%20wdt%3AP268%20%3FbnfId%20.%0D%0A%20%20%23%20langue%20fran%C3%A7aise%0D%0A%20%20%7B%3Fs%20wdt%3AP1412%20wd%3AQ150%20.%7D%20UNION%20%7B%3Fs%20wdt%3AP1412%20wd%3AQ1473289%20.%7D%0D%0A%20%20%23%20genre%0D%0A%20%20%7B%3Fs%20wdt%3AP21%20wd%3AQ6581072%20.%7D%20UNION%20%7B%3Fs%20wdt%3AP21%20wd%3AQ1097630%20.%7D%20UNION%20%7B%3Fs%20wdt%3AP21%20wd%3AQ1052281%20.%7D%20UNION%20%7B%3Fs%20wdt%3AP21%20wd%3AQ2449503%20.%7D%0D%0A%20%20%23%20ann%C3%A9e%20de%20naissance%0D%0A%20%20OPTIONAL%20%7B%20%3Fs%20wdt%3AP569%20%3Fdob.%7D%0D%0A%20%20BIND%28YEAR%28%3Fdob%29%20as%20%3Fyob%29%20.%0D%0A%20%20%23%20ann%C3%A9e%20de%20d%C3%A9c%C3%A8s%0D%0A%20%20OPTIONAL%20%7B%20%3Fs%20wdt%3AP570%20%3Fdod.%7D%0D%0A%20%20BIND%28YEAR%28%3Fdod%29%20as%20%3Fyod%29%20.%0D%0A%20%20%3Fsitelink%20schema%3Aabout%20%3Fs%20.%0D%0A%20%20OPTIONAL%20%7B%0D%0A%20%20%20%20%20%3Fs%20rdfs%3Alabel%20%3Fwriter%20filter%20%28lang%28%3Fwriter%29%20%3D%20%22fr%22%29.%0D%0A%20%20%7D%0D%0A%20%20OPTIONAL%20%7B%0D%0A%20%20%20%20%20%3Fpl%20rdfs%3Alabel%20%3Fplace%20filter%20%28lang%28%3Fplace%29%20%3D%20%22fr%22%29.%0D%0A%20%20%7D%0D%0A%20%20FILTER%28%28%28%3Fyob%3C%3D"+(parseInt(dYear)-15)+"%29%20%26%26%20%28%3Fyod%3E%3D"+(parseInt(bYear)+15)+"%29%29%20%7C%7C%20%28%28%3Fyob%3C%3D"+(parseInt(dYear)-15)+"%29%20%26%26%20%28%3Fyob%3E%3D"+(parseInt(bYear)-15)+"%29%29%20%29%0D%0A%7D%20GROUP%20BY%20%3Fplace%20%3Fwriter%20%3FbnfId%20%3Fyob%20%3Fyod%20ORDER%20BY%20DESC%28%3Flinkcount%29&format=json";
      $.get(query).done(function(data){      
         //console.log(data.results.bindings);
         (data.results.bindings).forEach(addWriter);      
      });
}

$(document).ready(function(){
   $("#resultatsAlterEgo").hide();
   
   $("#titreHeader").on("click",function(){
      location.href="./index.php";
   })
   
   $("h1,h2,.titresPage").css("font-family","\"Crimson Text\", Georgia, \"Times New Roman\", Times, serif");
   
   $("button#rechercheContenu").on("click",function(){
      alert("Un peu de patience, George sera bientôt opérationnelle pour répondre à vos demandes. Revenez sur ce site en juillet 2017 pour y trouver davantage de contenus et fonctionnalités !");
   })
   
   // When the button to send the query about the author name is clicked
   $("button#alterEgo").on("click",function(){
      $("#resultatsAlterEgo").html("");
      $("#resultatsAlterEgo").show("");
      var nomAuteur = $("#nomAuteur").val();
      nomAuteur = nomAuteur.replace(/ /g,"+");
      var query = "http://data.bnf.fr/sparql?default-graph-uri=&query=PREFIX+bio%3A+%3Chttp%3A%2F%2Fvocab.org%2Fbio%2F0.1%2F%3E%0D%0APREFIX+foaf%3A+%3Chttp%3A%2F%2Fxmlns.com%2Ffoaf%2F0.1%2F%3E%0D%0APREFIX+skos%3A+%3Chttp%3A%2F%2Fwww.w3.org%2F2004%2F02%2Fskos%2Fcore%23%3E%0D%0APREFIX+rdagroup2elements%3A+%3Chttp%3A%2F%2Frdvocab.info%2FElementsGr2%2F%3E%0D%0ASELECT+%28COUNT%28DISTINCT+%3Frel%29+as+%3Frelcount%29+%3Fauteur+%3Fjour+%3Fdate1+%3Fdate2+%3Fnote+%3Fprenom+%3Fnom+%3Fgenre+%3Fnomfamille%0D%0AWHERE+%7B%0D%0A+%7B+%3Fauteur+foaf%3Aname+%22"+nomAuteur+"%22.%7D%0D%0A++%3Fauteur+foaf%3AgivenName+%3Fprenom.%0D%0A++%3Fauteur+foaf%3Aname+%3Fnomfamille.%0D%0A++%3Fauteur+rdagroup2elements%3AbiographicalInformation+%3Fnote.%0D%0A++%3Fauteur+bio%3Abirth+%3Fdate1.%0D%0A++%3Fauteur+bio%3Adeath+%3Fdate2.%0D%0A++%3Fauteur+%3Frel+%3Fbla.%0D%0A++OPTIONAL+%7B%0D%0A++++%3Fauteur+foaf%3Agender+%3Fgenre.%0D%0A++++%3Fauteur+foaf%3Aname+%3Fnom.%0D%0A++++%3Fauteur++foaf%3Abirthday+%3Fjour.%0D%0A++++%3Fauteur+bio%3Abirth+%3Fdate1.%0D%0A++++%3Fauteur+bio%3Adeath+%3Fdate2.%0D%0A++%7D%0D%0A%7D%0D%0AGROUP+BY+%3Fauteur+%3Fjour+%3Fdate1+%3Fdate2+%3Fnote+%3Fprenom+%3Fnom+%3Fgenre+%3Fnomfamille%0D%0AORDER+BY+DESC%28%3Frelcount%29%0D%0A%0D%0A&format=application%2Fsparql-results%2Bjson&timeout=0&should-sponge=&debug=on";
      query = "https://query.wikidata.org/bigdata/namespace/wdq/sparql?query=%23Ann%C3%A9es%20de%20naissance%20et%20d%C3%A9c%C3%A8s%20de%20Moli%C3%A8re%0ASELECT%20%3Fitem%20%3FitemLabel%20%3F_image%20%3Fprenom%20%3Fdob%20%3Fdod%20%3Fyob%20%3Fyod%20%3Flink%0AWHERE%0A%7B%0A%20%20%3Fitem%20%3Flink%20%22"+nomAuteur+"%22%40fr.%0A%20%20%3Fitem%20wdt%3AP735%20%3Fprenom.%0A%20%20%3Fitem%20wdt%3AP569%20%3Fdob.%0A%20%20OPTIONAL%20%7B%20%3Fitem%20wdt%3AP570%20%3Fdod.%7D%0A%20%20BIND%28YEAR%28%3Fdob%29%20as%20%3Fyob%29%20.%0A%20%20BIND%28YEAR%28%3Fdod%29%20as%20%3Fyod%29%20.%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22fr%22.%20%7D%0A%20%20OPTIONAL%20%7B%20%3Fitem%20wdt%3AP18%20%3F_image.%20%7D%0A%7D&format=json";
      //https://query.wikidata.org/#%23Ann%C3%A9es%20de%20naissance%20et%20d%C3%A9c%C3%A8s%20de%20Moli%C3%A8re%0ASELECT%20%3Fitem%20%3FitemLabel%20%3F_image%20%3Fprenom%20%3Fdob%20%3Fdod%20%3Fyob%20%3Fyod%20%3Flink%0AWHERE%0A%7B%0A%20%20%3Fitem%20%3Flink%20%22Moli%C3%A8re%22%40fr.%0A%20%20%3Fitem%20wdt%3AP735%20%3Fprenom.%0A%20%20%3Fitem%20wdt%3AP569%20%3Fdob.%0A%20%20%3Fitem%20wdt%3AP570%20%3Fdod.%0A%20%20BIND%28YEAR%28%3Fdob%29%20as%20%3Fyob%29%20.%0A%20%20BIND%28YEAR%28%3Fdod%29%20as%20%3Fyod%29%20.%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22fr%22.%20%7D%0A%20%20OPTIONAL%20%7B%20%3Fitem%20wdt%3AP18%20%3F_image.%20%7D%0A%7D
      //console.log(query);
      $.get(query).done(getBirthDeathDateResults).fail(function(){
         console.log("Aucun résultat trouvé");
      });
   })
   
   // When the "enter" key is pressed just after writing the author name in the query field:
   $('#nomAuteur').keyup(function(e) {    
      if(e.keyCode == 13) {
         $("button#alterEgo").trigger("click");
      }
   });
})
