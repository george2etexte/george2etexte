<?php
session_start();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
   <title>George, le deuxième texte</title>
   <meta http-equiv="Content-Type" content="text/html; charset=UTF8" />
   <link rel="stylesheet" href="./style.css" />
   <link href="./dist/css/bootstrap.css" rel="stylesheet">
   <link rel="shortcut icon" href="./images/icone_george2etexte.ico" type="images/x-icon" />  
</head>

<body style="background-color:white;font-size:12pt;">
<SCRIPT TYPE="text/javascript" SRC="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></SCRIPT>
<SCRIPT>
$(document).ready(function(){
   $("h2").hide();
})
</SCRIPT>

<?php
include("./header.php");
?>

<!--<hr/>-->

<div  style="background-color:#F5F5F5;margin-top:20px;padding:20px;">

<div class="container">



<?php
include("./parameters.php");
include("./functions.php");
    
    function chargerClasse($classe){
        require 'test/Class/'.$classe.'.php';
    }
    spl_autoload_register('chargerClasse');
    
    if(isset($_GET['id'])){
        $utilisatrice = intval($_GET['id']);
    } else {
        $utilisatrice = 0;
    }
    
    if(isset($_GET['autId'])){
        $autrice = intval($_GET['autId']);
    } else {
        $autrice = 0;
    }
    
    if(isset($_GET['joursansE'])){
        $joursansE = intval($_GET['joursansE']);
    } else {
        $joursansE = 0;
    }
    
    
    
    $managerU = new UtilisatriceManager($bdd);
    $managerE = new ExtraitManager($bdd);
    $managerT = new TagManager($bdd);
    $managerN = new NotionManager($bdd);
    $managerP = new PeriodeManager($bdd);

?>

<div class="panel panel-default" style="text-align:center;padding:20px;">
  <div class="panel-body">
  <h1 class="form-signin-heading">Extraits 
  <?php
  if($utilisatrice !== 0){
     $util = $managerU->getUtilisatriceById($utilisatrice);
      $prenom = $util->getPrenom_utilisatrice();
      $nom = $util->getNom_utilisatrice();
     echo " proposés par ".$util->fullName()."</h1>";
  } else {
     if($autrice !== 0){
        $aut = $managerE->getAutriceByExtrait($autrice);
        echo " écrits par ".fullName($aut["prenom_autrice"],$aut["nom_autrice"])."</h1>";
     } else {
        if($joursansE !== 0){
           echo " disponibles (noms d'autrices sans la lettre e)</h1>";
        } else { 
           echo " disponibles</h1>";
        }
     }
  }
      if($_GET == array()){
      ?>
      </h1><br>
      <form action="recherche_extraits.php" method="get">
          <fieldset><legend>Recherche par mot: </legend>
              <input type="search" name="donneescherche" id="donneescherche" class="form-control" required placeholder="Sujet de la recherche"><br><br>
              
              <input type="image" src="images/magnifier.svg" width="80" height="80" value="Validez">
          </fieldset>
      </form>
      <hr>
      <form action="recherche_extraits_alternative.php" method="get">
          <fieldset><legend>Recherche experte: </legend>
        <span>Je cherche un texte <select name="periode" id="periode">
                  <option value="">-- Si vous le souhaitez, choisissez une période --</option>
                  <?php
          $managerP->getAllPeriodesDet();
                  ?>
              </select> dans le genre littéraire <select name="genre" id="genre">
                  <option value="">-- Si vous le souhaitez, choisissez un genre littéraire --</option>
                  <?php
          $managerT->getAllTagsByType(2);
            ?>
              </select>, en rapport avec la notion <select name="notion" id="notion">
                  <option value="">-- Si vous le souhaitez, choisissez une notion --</option>
                  <?php
          $managerN->getAllNotionsDet();
                  ?>
            </select> et la thématique <select name="tag" id="tag">
                  <option value="">-- Si vous le souhaitez, choisissez une thématique --</option>
                  <?php
          $managerT->getAllTagsByType(0);
                  ?>
              </select>
          </span><br><br>
              
              <input type="image" src="images/magnifier.svg" width="80" height="80" value="Validez">
          </fieldset>
              
      </form>
      <?php
  }
  ?>
  <hr/>
<?php

  // Informations sur l'utilisatrice
  if($utilisatrice !== 0){
     echo "<p>";
     $util->displayUser();
     echo "<br/><br/></p>";
  }

  // Informations sur l'autrice
  if($autrice !== 0){
     echo "<p>";
     displayAuthor($aut);
     echo "<br/><br/></p>";
  }
      
      $extraits2 = $managerE->getAllExtraitsByParameters($utilisatrice, $autrice, $joursansE);
  
  //Affiche une partie des extraits
      if($utilisatrice == 0 && $autrice == 0 && $joursansE == 0){
          $managerE->accueilExtraits($utilisatrice);
      } else {
      // Affiche tous les articles, en supprimant la colonne contributrice si la contributrice est connue
  $managerE->allTexts($utilisatrice, $autrice, $joursansE, 0, array());
      }
      if($utilisatrice !== 0 || $autrice !== 0){
          ?>
          <a href="extraits.php">Retourner voir tous les extraits</a><br><br>
          <a href="index.php">Retour à l'accueil</a>
          <?php
      } else {
          ?>
          <a href="index.php">Retour</a>
          <?php
      }
?>
  </div>
</div>
    </div>
    </div>
  
<?php
include("./footer.php");
?> 
</body>
</html>
