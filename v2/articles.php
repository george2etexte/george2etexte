<?php
session_start():
?>
<!DOCTYPE html>
<html>
<head>
   <title>George, le deuxième texte</title>
   <meta http-equiv="Content-Type" content="text/html; charset=UTF8" />
   <link rel="stylesheet" href="./style.css" />
   <link href="./dist/css/bootstrap.css" rel="stylesheet">
   <link rel="shortcut icon" href="./images/icone_george2etexte.ico" type="images/x-icon" />  
</head>

<body style="background-color:white;font-size:12pt;">
<SCRIPT TYPE="text/javascript" SRC="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></SCRIPT>
<SCRIPT>
$(document).ready(function(){
   $("h2").hide();
   $(".titresPage").on("click",function(e){
      e.stopPropagation();
      $("#home").trigger("click");
   });
})
</SCRIPT>

<div style="text-align:center;" class="titresPage">
<a id="home" href="index.php"></a><span style="font-size:24pt">George</span><br/>
<span style="font-size:18pt">* le <span style="background-color:#8282D2;">deuxième texte</span></span>
</div>

<!--<hr/>-->

<div  style="background-color:#F5F5F5;margin-top:20px;padding:20px;">

<div class="container">

<!--
<div class="panel panel-default" style="text-align:center;padding:20px;">
  <div class="panel-body" style="font-family:Calibri;">
  <p>
  <i>George - Le deuxième texte</i> est une plateforme web,
  conçue lors du <a href="https://forum.etalab.gouv.fr/t/a-propos-de-la-categorie-hackegalitefh/3445">#HackEgalitéFH</a>,
  qui met à disposition des professeur·e·s (et du grand public) une base de textes écrits tant par des femmes que par des hommes, de la manière la plus exhaustive et paritaire possible. L’objectif est de donner plus de visibilité aux autrices dans les programmes scolaires, afin que les jeunes puissent s’identifier à des figures fortes, sans distinction de genre.
  </p>
  
  <p>
  <b><i>George - Le deuxième texte</i> est en cours de construction.</i></b>
  </p>
  
  <p>Retrouvez <a href="https://twitter.com/Georgele2etexte">notre fil Twitter</a>,
  <a href="https://www.facebook.com/George-le-deuxi%C3%A8me-texte-994051650727500/">notre page Facebook</a>,
  <a href="./pdf/PREZ_George_2eme texte.pdf">notre présentation</a> et 
  les maquettes de la plateforme
  (<a href="./images/maquette_accueil.png">page d'accueil</a>, <a href="./images/maquette_article.png">page d'article</a>).
  </p>

  <p>
  <a href="https://twitter.com/Georgele2etexte"><img src="./images/logo_twitter.png"></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  <a href="https://www.facebook.com/George-le-deuxi%C3%A8me-texte-994051650727500/"><img src="./images/logo_facebook.png"></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  <a href="https://twitter.com/Georgele2etexte/status/838315695846932480"><img src="./images/bouton_contribuer.png"></a>
  </p>
  
  </div>
</div>
-->


<?php
include("parameters.php");
include("functions.php");
    
    function chargerClasse($classe){
        require 'test/Class/'.$classe.'.php';
    }
    spl_autoload_register('chargerClasse');

if (isset($_GET["id"])) {
    $id_article = intval($_GET["id"]);
} else {
    $id_article = 0;
}
//Load latest article
$sql = 'SELECT * FROM 2etexte_article,2etexte_utilisatrice,2etexte_autrice WHERE 2etexte_utilisatrice.id_utilisatrice=2etexte_article.id_utilisatrice_article AND 2etexte_article.id_article='.$id_article.';';
$req = mysqli_query($link, $sql)
   or die('Erreur SQL !<br>'.$sql.'<br>'.mysqli_error($link));
$data = mysqli_fetch_assoc($req);
?>



<div class="panel panel-default" style="text-align:center;padding:20px;">
  <div class="panel-body">
  <h1 class="form-signin-heading"><?php echo $data["titre_article"]; ?></h1>
  <hr/>
<?php
   displayArticle($data["id_article"],$link);
?>
  </div>
</div>

  
<?php
include("footer.php");
?> 
</body>
</html>
