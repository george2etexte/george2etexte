<?php
session_start();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="dist/css/bootstrap.css">
    <link rel="stylesheet" href="dist/css/normalize.css">
    <link rel="stylesheet" href="test/style.css">
    <title>Updating</title>
</head>
<body>
    <?php
    function chargerClasse($classe){
        require "test/Class/".$classe.".php";
    }
    spl_autoload_register('chargerClasse');
    include('parameters.php');
    $manager = new UtilisatriceManager($bdd);
    $user = $manager->getUtilisatriceById($_SESSION['id']);
    $photo = $user->getPhoto_utilisatrice();
    
    $prenom = strip_tags(htmlspecialchars($_POST['prenom']));
    $nom = strip_tags(htmlspecialchars($_POST['nom']));
    $mail = strip_tags(htmlspecialchars($_POST['mailing']));
    
    if(isset($_POST['presentation'])){
       $presentation = strip_tags(htmlspecialchars($_POST['presentation'])); 
    }
    
    $lien = strip_tags(htmlspecialchars($_POST['lien_site']));
    
    if(isset($_POST['institution'])){
        $institution = strip_tags(htmlspecialchars($_POST['institution']));
    }
    
    if(isset($_POST['etablissement'])){
        $etablissement = strip_tags(htmlspecialchars($_POST['etablissement']));
    }
    
    if(isset($_POST['academie'])){
        $academie = strip_tags(htmlspecialchars($_POST['academie']));
    }
    
    
    
    if(isset($_FILES['photo']['name']) && !empty($_FILES['photo']['name']) && isset($_POST['mdpin']) && isset($_POST['confirmation'])){
        
        $mdp = strip_tags(htmlspecialchars($_POST['mdpin']));
    $confirmation = strip_tags(htmlspecialchars($_POST['confirmation']));
        
        if($mdp == $confirmation){
            if(!preg_match('#^http#', $photo) && $photo !== 'photos/anonyme.png'){
                unlink($photo);
            }
        $contenu = 'photos/';
        $tmp_name = $_FILES['photo']['tmp_name'];
        if(is_uploaded_file($tmp_name)){
            $tableau_fichier = explode(".", $_FILES['photo']['name']);
            $nouveau_nom = hash("sha256", $tableau_fichier[0] . rand()) . "." . $tableau_fichier[1];
            $adresse_photo = $contenu . $nouveau_nom;
            move_uploaded_file($tmp_name, $adresse_photo);
            $adresse = 'photos/' . $nouveau_nom;
        }
            
            $hashage = password_hash($mdp . $salage, PASSWORD_DEFAULT);
            
            if(password_verify($mdp . $salage, $hashage)){
                $donnees = array('id_utilisatrice' => intval($_SESSION['id']), 'prenom_utilisatrice' => $prenom, 'nom_utilisatrice' => $nom, 'mail_utilisatrice' => $mail, 'presentation_utilisatrice' => $presentation, 'lien_utilisatrice' => $lien, 'photo_utilisatrice' => $adresse, 'adresse_institutionnelle' => $institution, 'etablissement' => $etablissement, 'academie' => $academie, 'mdp_utilisatrice' => $hashage);
            }
        } else {
            echo "<p>Le mot de passe de confirmation n'est pas bon</p>";
        }
    } else {
        
        if(isset($_FILES['photo']['name']) && !empty($_FILES['photo']['name'])){
            
            if(!preg_match('#^http#', $photo) && $photo !== 'photos/anonyme.png'){
                unlink($photo);
            }
            
        $contenu = 'photos/';
        $tmp_name = $_FILES['photo']['tmp_name'];
        if(is_uploaded_file($tmp_name)){
            $tableau_fichier = explode(".", $_FILES['photo']['name']);
            $nouveau_nom = hash("sha256", $tableau_fichier[0] . rand()) . "." . $tableau_fichier[1];
            $adresse_photo = $contenu . $nouveau_nom;
            move_uploaded_file($tmp_name, $adresse_photo);
            $adresse = 'photos/' . $nouveau_nom;
        }
            $donnees = array('id_utilisatrice' => intval($_SESSION['id']), 'prenom_utilisatrice' => $prenom, 'nom_utilisatrice' => $nom, 'mail_utilisatrice' => $mail, 'presentation_utilisatrice' => $presentation, 'lien_utilisatrice' => $lien, 'photo_utilisatrice' => $adresse, 'adresse_institutionnelle' => $institution, 'etablissement' => $etablissement, 'academie' => $academie);
            
        } else if(isset($_POST['avatar']) && !empty($_POST['avatar']) && isset($_POST['mdpin']) && isset($_POST['confirmation'])){
            
                    
                if(!preg_match('#^http#', $photo) && $photo !== 'photos/anonyme.png'){
                unlink($photo);
            }
            
            $hashmail = md5(strtolower(trim($mail)));
            $adresse = 'https://www.gravatar.com/avatar/'. $hashmail .'?s=200&r=g&d=wavatar';
            
            $mdp = strip_tags(htmlspecialchars($_POST['mdpin']));
    $confirmation = strip_tags(htmlspecialchars($_POST['confirmation']));
            
            if($mdp == $confirmation){
            $hashage = password_hash($mdp . $salage, PASSWORD_DEFAULT);
            
            if(password_verify($mdp . $salage, $hashage)){
                $donnees = array('id_utilisatrice' => intval($_SESSION['id']), 'prenom_utilisatrice' => $prenom, 'nom_utilisatrice' => $nom, 'mail_utilisatrice' => $mail, 'presentation_utilisatrice' => $presentation, 'lien_utilisatrice' => $lien, 'photo_utilisatrice' => $adresse, 'adresse_institutionnelle' => $institution, 'etablissement' => $etablissement, 'academie' => $academie, 'mdp_utilisatrice' => $hashage);
            }
        } else {
            echo "<p>Le mot de passe de confirmation n'est pas bon</p>";
        }
        } else if(isset($_POST['avatar']) && !empty($_POST['avatar'])) {
                $hashmail = md5(strtolower(trim($mail)));
            $adresse = 'https://www.gravatar.com/avatar/'. $hashmail .'?s=200&r=g&d=wavatar';
                
                if(!preg_match('#^http#', $photo) && $photo !== 'photos/anonyme.png'){
                unlink($photo);
            }
                
                $donnees = array('id_utilisatrice' => intval($_SESSION['id']), 'prenom_utilisatrice' => $prenom, 'nom_utilisatrice' => $nom, 'mail_utilisatrice' => $mail, 'presentation_utilisatrice' => $presentation, 'lien_utilisatrice' => $lien, 'photo_utilisatrice' => $adresse, 'adresse_institutionnelle' => $institution, 'etablissement' => $etablissement, 'academie' => $academie);
                
        } else if(isset($_POST['mdpin']) && isset($_POST['confirmation'])) {
            
            $mdp = strip_tags(htmlspecialchars($_POST['mdpin']));
    $confirmation = strip_tags(htmlspecialchars($_POST['confirmation']));
            
            if($mdp == $confirmation){
                $hashage = password_hash($mdp . $salage, PASSWORD_DEFAULT);
            
            if(password_verify($mdp . $salage, $hashage)){
                $donnees = array('id_utilisatrice' => intval($_SESSION['id']), 'prenom_utilisatrice' => $prenom, 'nom_utilisatrice' => $nom, 'mail_utilisatrice' => $mail, 'presentation_utilisatrice' => $presentation, 'lien_utilisatrice' => $lien, 'adresse_institutionnelle' => $institution, 'etablissement' => $etablissement, 'academie' => $academie, 'mdp_utilisatrice' => $hashage);
            }
            } else {
                echo "<p>Le mot de passe de confirmation n'est pas bon</p>";
            }
            
        } else {
            $donnees = array('id_utilisatrice' => intval($_SESSION['id']), 'prenom_utilisatrice' => $prenom, 'nom_utilisatrice' => $nom, 'mail_utilisatrice' => $mail, 'presentation_utilisatrice' => $presentation, 'lien_utilisatrice' => $lien, 'adresse_institutionnelle' => $institution, 'etablissement' => $etablissement, 'academie' => $academie);
            
        }
    }
    
    $utilisatrice = new Utilisatrice($donnees);
    $manager->updateUtilisatrice($utilisatrice);
    echo "Profil mis à jour !<br>";
    ?>
    <a href="profil_modif.php">Retour</a>
</body>
</html>