<?php
session_start();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Résultats des recherches</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF8" />
   <link rel="stylesheet" href="./style.css" />
   <link href="./dist/css/bootstrap.css" rel="stylesheet">
   <link rel="shortcut icon" href="./images/icone_george2etexte.ico" type="images/x-icon" />
</head>
<body style="background-color:white;font-size:12pt;">
    <SCRIPT TYPE="text/javascript" SRC="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></SCRIPT>
<SCRIPT>
$(document).ready(function(){
   $("h2").hide();
})
</SCRIPT>
<?php
    include('header.php');
    include('parameters.php');
    include('functions.php');
    
    function chargerClasse($classe){
        require 'test/Class/'.$classe.'.php';
    }
    spl_autoload_register('chargerClasse');
    
    $manager = new ExtraitManager($bdd);
    $regex = $_GET['donneescherche'];
    
    ?>
    <div style="background-color:#F5F5F5;margin-top:20px;padding:20px;">
            <div class="container">
                 <h1>Résultats de la recherche: <?php echo strip_tags(htmlspecialchars($regex)); ?> </h1>
                  <div class="panel panel-default" style="text-align:center;padding:20px;">
        <div class="panel-body">
          <?php
            
                $manager->allTexts(0, 0, 0, $regex, array());
            ?>
            <a href="extraits.php">Retour</a>
        </div>
    </div>  
                </div>
            </div>
    <?php
    include('footer.php');
    ?>
</body>
</html>