<?php
class NotionManager{
    private $db;
    
    public function setDb(PDO $db){
        $this->db = $db;
    }
    
    public function __construct(PDO $db){
        $this->setDb($db);
    }
    
    public function getAllNotions(){
        $recherche = $this->db->query('SELECT * FROM 2etexte_notion');
        while($donnees = $recherche->fetch()){
            echo "<option value=\"".$donnees['id_notion']."\">".ucfirst($donnees['nom_notion'])."</option>";
        }
    }
    
    public function getAllNotionsDet(){
        $recherche = $this->db->query('SELECT * FROM 2etexte_notion');
        while($donnees = $recherche->fetch()){
            echo "<option value=\"".$donnees['id_notion']."\">".$donnees['nom_prep_notion']."</option>";
        }
    }
}

?>