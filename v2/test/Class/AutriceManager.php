<?php
class AutriceManager{
    private $db;
    
    public function setDb(PDO $db){
        $this->db = $db;
    }
    
    public function __construct(PDO $db){
        $this->setDb($db);
    }
    
    public function statAutrice(){
        $cherche = $this->db->prepare('SELECT * FROM 2etexte_autrice');
        $cherche->execute();
        $stat = $cherche->rowCount();
        if($stat > 1){
            return "<li>".$stat." autrices à absolument connaître</li>";
        } else {
            return "<li>".$stat." autrice à absolument connaître</li>";
        }
    }
    
    public function getAutriceById($id){
        $cherche = $this->db->prepare('SELECT * FROM 2etexte_autrice WHERE id_autrice = :id');
        $cherche->execute(array('id' => intval($id)));
        $donnees = $cherche->fetch();
        $autrice = new Autrice($donnees);
        return $autrice;
    }
    
    
    
    public function afficheOptionsAutrices(){
        $cherche = $this->db->query('SELECT * FROM 2etexte_autrice');
        while($donnees = $cherche->fetch()){
            echo "<option value=\"".intval($donnees['id_autrice'])."\">".$donnees['prenom_autrice']." ".$donnees['nom_autrice']."</option>";
        }
    }
    
    public function insertAutrice(Autrice $autrice){
        $insert = $this->db->prepare('INSERT INTO 2etexte_autrice(prenom_autrice, nom_autrice, sexe, naissance, deces, naissance_str, deces_str, minibio, image_autrice, image_autrice_source, id_bnf) VALUES(:prenom, :nom, :sexe, :naissance, :deces, :strn, :strd, :minibio, :imgautrice, :imgsource, :idbnf)');
        $insert->execute(array('prenom' => $autrice->prenom_autrice,
                              'nom' => $autrice->nom_autrice,
                              'sexe' => $autrice->sexe,
                              'naissance' => $autrice->naissance,
                              'deces' => $autrice->deces,
                              'strn' => $autrice->naissance_str,
                              'strd' => $autrice->deces_str,
                              'minibio' => $autrice->minibio,
                              'imgautrice' => $autrice->image_autrice,
                              'imgsource' => $autrice->image_autrice_source,
                              'idbnf' => $autrice->id_bnf));
    }
}
?>