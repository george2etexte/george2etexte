<?php
class UtilisatriceManager {
    private $db;
    
    public function setDb(PDO $db){
        $this->db = $db;
    }
    
    public function __construct($db){
        $this->setDb($db);
    }
    
    public function getAllUtilisatrices(){
        $query = $this->db->query('SELECT * FROM 2etexte_utilisatrice');
        while($donnees = $query->fetch()){
            echo "<div class=\"media\">
            <div class=\"media-left\">
            <img class=\"roundedImage\" src=\"".$donnees['photo_utilisatrice']."\" alt=\"Photo de ".$donnees['prenom_utilisatrice']." ".$donnees['nom_utilisatrice']."\" title=\"Photo de ".$donnees['prenom_utilisatrice']." ".$donnees['nom_utilisatrice']."\">
            </div>
            <div class=\"media-right\">
            <a href=\"profil_public.php?id=".$donnees['id_utilisatrice']."\">".$donnees['prenom_utilisatrice']." ".$donnees['nom_utilisatrice']."</a>
            </div>
            </div>";
        }
    }
    
    public function getAllUtilisatricesByRegex($regex){
        $query = $this->db->query('SELECT * FROM 2etexte_utilisatrice WHERE prenom_utilisatrice REGEXP "^'.$regex.'" OR nom_utilisatrice REGEXP "^'.$regex.'" OR CONCAT(prenom_utilisatrice, " ", nom_utilisatrice) REGEXP "^'.$regex.'" OR CONCAT(prenom_utilisatrice, "", nom_utilisatrice) REGEXP "^'.$regex.'" OR CONCAT(nom_utilisatrice, " ", prenom_utilisatrice) REGEXP "^'.$regex.'" OR CONCAT(nom_utilisatrice, "", prenom_utilisatrice) REGEXP "^'.$regex.'"');
        $resultats = 0;
        while($donnees = $query->fetch()){
            $resultats++;
            echo "<div class=\"media\">
            <div class=\"media-left\">
            <img class=\"roundedImage\" src=\"".$donnees['photo_utilisatrice']."\" alt=\"Photo de ".$donnees['prenom_utilisatrice']." ".$donnees['nom_utilisatrice']."\" title=\"Photo de ".$donnees['prenom_utilisatrice']." ".$donnees['nom_utilisatrice']."\">
            </div>
            <div class=\"media-right\">
            <a href=\"profil_public.php?id=".$donnees['id_utilisatrice']."\">".$donnees['prenom_utilisatrice']." ".$donnees['nom_utilisatrice']."</a>
            </div>
            </div>";
        }
        
        
        if($resultats == 0){
            echo "<p>Aucun résultat pour votre recherche</p>";
        } else {
            if($resultats > 1){
            $pluriel = "s";
        } else {
            $pluriel = "";
        }
            echo '<div style="text-align:right">&rarr; '.$resultats.' extrait'.$pluriel.' au total</div>';
        }
    }
    
    public function getUtilisatriceById($id){
        $query = $this->db->prepare('SELECT * FROM 2etexte_utilisatrice WHERE id_utilisatrice = :id');
        $query->execute(array('id' => intval($id)));
        $donnees = $query->fetch();
        $utilisatrice = new Utilisatrice($donnees);
        return $utilisatrice;
    }
    
    public function getUtilisatriceByMail($mail){
        $query = $this->db->prepare('SELECT * FROM 2etexte_utilisatrice WHERE mail_utilisatrice = :mail');
        $query->execute(array('mail' => $mail));
        $donnees = $query->fetch();
        return intval($donnees['id_utilisatrice']);
    }
    
    public function getUtilisatriceByMailMdp2($mail){
        $query = $this->db->prepare('SELECT * FROM 2etexte_utilisatrice WHERE mail_utilisatrice = :mail');
        $query->execute(array('mail' => $mail));
        $donnees = $query->fetch();
        return $donnees['mdp_utilisatrice'];
    }
    
    public function getUtilisatriceByMailMdp($mail, $mdp){
        $query = $this->db->prepare('SELECT * FROM 2etexte_utilisatrice WHERE mail_utilisatrice = :mail AND mdp_utilisatrice = :mdp');
        $query->execute(array('mail' => $mail,
                             'mdp' => $mdp));
        $donnees = $query->fetch();
        return intval($donnees['id_utilisatrice']);
    }
    
    public function getUtilisatriceByWebsite($lien){
        if($lien !== ""){
            $query = $this->db->prepare('SELECT * FROM 2etexte_utilisatrice WHERE lien_utilisatrice = :lien');
        $query->execute(array('lien' => $lien));
        $donnees = $query->fetch();
        return intval($donnees['id_utilisatrice']);
        } else {
            return 0;
        }
    }
    
    public function getUtilisatriceByToken($token){
        $query = $this->db->prepare('SELECT * FROM 2etexte_utilisatrice WHERE token_utilisatrice = :token');
        $query->execute(array('token' => $token));
        $donnees = $query->fetch();
        return intval($donnees['id_utilisatrice']);
    }
    
    public function insertUtilisatrice(Utilisatrice $utilisatrice){
        $prenom = $utilisatrice->getPrenom_utilisatrice();
        $nom = $utilisatrice->getNom_utilisatrice();
        $mail = $utilisatrice->getMail_utilisatrice();
        $presentation = $utilisatrice->getPresentation_utilisatrice();
        $lien = $utilisatrice->getLien_utilisatrice2();
        $photo = $utilisatrice->getPhoto_utilisatrice();
        $institution = $utilisatrice->getAdresse_institutionnelle();
        $etablissement = $utilisatrice->getEtablissement();
        $academie = $utilisatrice->getAcademie();
        $mdp = $utilisatrice->getMdp_utilisatrice();
        
        $insert = $this->db->prepare('INSERT INTO 2etexte_utilisatrice(prenom_utilisatrice, nom_utilisatrice, mail_utilisatrice, presentation_utilisatrice, lien_utilisatrice, photo_utilisatrice, adresse_institutionnelle, etablissement, academie, mdp_utilisatrice) VALUES(:prenom, :nom, :mail, :presentation, :lien, :photo, :institution, :etablissement, :academie, :mdp)');
        $insert->execute(array('prenom' => $prenom,
                              'nom' => $nom,
                              'mail' => $mail,
                              'presentation' => $presentation,
                              'lien' => $lien,
                              'photo' => $photo,
                              'institution' => $institution,
                              'etablissement' => $etablissement,
                              'academie' => $academie,
                              'mdp' => $mdp));
    }
    
    public function updateUtilisatrice(Utilisatrice $utilisatrice){
        $id = $utilisatrice->getId_utilisatrice();
        $prenom = $utilisatrice->getPrenom_utilisatrice();
        $nom = $utilisatrice->getNom_utilisatrice();
        $mail = $utilisatrice->getMail_utilisatrice();
        $presentation = $utilisatrice->getPresentation_utilisatrice();
        
        if($utilisatrice->getLien_utilisatrice2() !== null){
            $lien = $utilisatrice->getLien_utilisatrice2();
        } else {
            $lien = "";
        }
        
        $adresse = $utilisatrice->getAdresse_institutionnelle();
        $etablissement = $utilisatrice->getEtablissement();
        $academie = $utilisatrice->getAcademie();
        
        if($utilisatrice->getMdp_utilisatrice() !== null){
            $mdp = $utilisatrice->getMdp_utilisatrice();
        }
        
        if($utilisatrice->getPhoto_utilisatrice() !== null){
            $photo = $utilisatrice->getPhoto_utilisatrice();
        }
        
        if(isset($mdp) && isset($photo)){
            $maj = $this->db->prepare('UPDATE 2etexte_utilisatrice SET prenom_utilisatrice = :prenom, nom_utilisatrice = :nom, mail_utilisatrice = :mail, presentation_utilisatrice = :presentation, lien_utilisatrice = :lien, photo_utilisatrice = :photo, adresse_institutionnelle = :adresse, etablissement = :etablissement, academie = :academie, mdp_utilisatrice = :mdp WHERE id_utilisatrice = :id');
            $maj->execute(array('id' => $id,
                               'prenom' => $prenom,
                               'nom' => $nom,
                               'mail' => $mail,
                               'presentation' => $presentation,
                               'lien' => $lien,
                               'photo' => $photo,
                               'adresse' => $adresse,
                               'etablissement' => $etablissement,
                               'academie' => $academie,
                               'mdp' => $mdp));
        } else {
            if(isset($photo)){
                $maj = $this->db->prepare('UPDATE 2etexte_utilisatrice SET prenom_utilisatrice = :prenom, nom_utilisatrice = :nom, mail_utilisatrice = :mail, presentation_utilisatrice = :presentation, lien_utilisatrice = :lien, photo_utilisatrice = :photo, adresse_institutionnelle = :adresse, etablissement = :etablissement, academie = :academie WHERE id_utilisatrice = :id');
            $maj->execute(array('id' => $id,
                               'prenom' => $prenom,
                               'nom' => $nom,
                               'mail' => $mail,
                               'presentation' => $presentation,
                               'lien' => $lien,
                               'photo' => $photo,
                               'adresse' => $adresse,
                               'etablissement' => $etablissement,
                               'academie' => $academie));
            } else if(isset($mdp)) {
                $maj = $this->db->prepare('UPDATE 2etexte_utilisatrice SET prenom_utilisatrice = :prenom, nom_utilisatrice = :nom, mail_utilisatrice = :mail, presentation_utilisatrice = :presentation, lien_utilisatrice = :lien, adresse_institutionnelle = :adresse, etablissement = :etablissement, academie = :academie, mdp_utilisatrice = :mdp WHERE id_utilisatrice = :id');
            $maj->execute(array('id' => $id,
                               'prenom' => $prenom,
                               'nom' => $nom,
                               'mail' => $mail,
                               'presentation' => $presentation,
                               'lien' => $lien,
                               'adresse' => $adresse,
                               'etablissement' => $etablissement,
                               'academie' => $academie,
                               'mdp' => $mdp));
            } else {
                $maj = $this->db->prepare('UPDATE 2etexte_utilisatrice SET prenom_utilisatrice = :prenom, nom_utilisatrice = :nom, mail_utilisatrice = :mail, presentation_utilisatrice = :presentation, lien_utilisatrice = :lien, adresse_institutionnelle = :adresse, etablissement = :etablissement, academie = :academie WHERE id_utilisatrice = :id');
            $maj->execute(array('id' => $id,
                               'prenom' => $prenom,
                               'nom' => $nom,
                               'mail' => $mail,
                               'presentation' => $presentation,
                               'lien' => $lien,
                               'adresse' => $adresse,
                               'etablissement' => $etablissement,
                               'academie' => $academie));
            }
        }
    }
    
    public function updateToken($id, $token){
        $maj = $this->db->prepare('UPDATE 2etexte_utilisatrice SET token_utilisatrice = :token WHERE id_utilisatrice = :id');
        $maj->execute(array('id' => $id,
                           'token' => $token));
    }
    
    public function updateMdp($id, $mdp){
        $maj = $this->db->prepare('UPDATE 2etexte_utilisatrice SET mdp_utilisatrice = :mdp WHERE id_utilisatrice = :id');
        $maj->execute(array('id' => $id,
                           'mdp' => $mdp));
    }
    
    public function statUtil(){
        $cherche = $this->db->prepare('SELECT * FROM 2etexte_utilisatrice');
        $cherche->execute();
        $stat = $cherche->rowCount();
        if($stat > 1){
            return "<li>".$stat." utilisateurs inscrits</li>";
        } else {
            return "<li>".$stat." utilisateur inscrit</li>";
        }
    }
    
}
?>