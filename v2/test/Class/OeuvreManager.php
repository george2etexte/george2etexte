<?php
class OeuvreManager{
    private $db;
    
    public function setDb(PDO $db){
        $this->db = $db;
    }
    
    public function __construct(PDO $db){
        $this->setDb($db);
    }
    
    public function statOeuvre(){
        $cherche = $this->db->prepare('SELECT * FROM 2etexte_oeuvre');
        $cherche->execute();
        $stat = $cherche->rowCount();
        if($stat > 1){
            return "<li>".$stat." oeuvres à découvrir</li>";
        } else {
            return "<li>".$stat." oeuvre à découvrir</li>";
        }
    }
    
    public function afficheOptions(){
        require 'Autrice.php';
        require 'AutriceManager.php';
        
        $managerA = new AutriceManager($this->db);
        
        $cherche = $this->db->query('SELECT * FROM 2etexte_oeuvre, 2etexte_autrice WHERE 2etexte_oeuvre.id_autrice_oeuvre = 2etexte_autrice.id_autrice ORDER BY 2etexte_autrice.nom_autrice');
        while($data = $cherche->fetch()){
            $id = intval($data['id_autrice']);
            $autrice = $managerA->getAutriceById($id);
            if(strlen($data["reference_oeuvre"])+strlen($data["nom_autrice"])+strlen($data["prenom_autrice"])+3>120){
        $add="...";
     }else{
        $add="";
     }
     echo "<option value=\"".$data["id_oeuvre"]."\">".$autrice->fullName()." - ".mb_substr($data["reference_oeuvre"],0,120-strlen($data["nom_autrice"])-strlen($data["prenom_autrice"])-5).$add."</option>";
        }
    }
    
    public function insertOeuvre(Oeuvre $oeuvre){
        $cherche = $this->db->prepare('INSERT INTO 2etexte_oeuvre(id_autrice_oeuvre, autrice_oeuvre, annee_oeuvre, reference_oeuvre, source_oeuvre, achat_oeuvre) VALUES(:idautrice, :autrice, :annee, :reference, :source, :achat)');
        $cherche->execute(array('idautrice' => $oeuvre->id_autrice_oeuvre, 'autrice' => $oeuvre->autrice_oeuvre, 'annee' => $oeuvre->annee_oeuvre, 'reference' => $oeuvre->reference_oeuvre, 'source' => $oeuvre->source_oeuvre, 'achat' => $oeuvre->achat_oeuvre));
    }
}
?>