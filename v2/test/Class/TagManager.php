<?php
class TagManager{
    private $db;
    
    public function setDb(PDO $db){
        $this->db = $db;
    }
    
    public function __construct(PDO $db){
        $this->setDb($db);
    }
    
    public function getAllTags(){
        $cherche = $this->db->query('SELECT * FROM 2etexte_tag ORDER BY nom_tag');
        
        while($donnees = $cherche->fetch()){
            echo "<option value=\"".$donnees['id_tag']."\">".$donnees['nom_tag']."</option>";
        }
    }
    
    public function getAllTagsByType($type){
        $cherche = $this->db->prepare('SELECT * FROM 2etexte_tag WHERE type_tag = :type');
        $cherche->execute(array('type' => $type));
        
        while($donnees = $cherche->fetch()){
            
            $premiercaractere = substr($donnees['nom_tag'], 0, 1);
            
            if(preg_match('#(A|E|I|O|U|Y)#', $premiercaractere)){
                $prep = "d'";
            } else {
                $prep = "de ";
            }
            
            echo "<option value=\"".$donnees['id_tag']."\">".$prep.lcfirst($donnees['nom_tag'])."</option>";
        }
    }
    
    public function getOneTagById($id){
        $cherche = $this->db->prepare('SELECT * FROM 2etexte_tag WHERE id_tag = :id');
        $cherche->execute(array('id' => $id));
        $donnees = $cherche->fetch();
        $tag = new Tag($donnees);
        return $tag;
    }
    
    public function formatTag($tag){
        $formatage = ['/[[:punct:]]/', '/[[:cntrl:]]/'];
        
        $chaine_format = trim(preg_replace($formatage, '', $tag));
        
        $chaine_format = ucfirst($chaine_format);
        
        return $chaine_format;
    }
    
    public function insertTag(Tag $tag){
        $insertiontag = $this->db->prepare('INSERT INTO 2etexte_tag(nom_tag, type_tag) VALUES(:nomtag, :typetag)');
        $insertiontag->execute(array('nomtag' => $tag->nom_tag,
                                    'typetag' => $tag->type_tag)) or die(print_r($insertiontag->errorInfo()));
    }
    
    public function insertTagsInExtrait($tags, $id_extrait){
        date_default_timezone_set('Europe/Paris');
        
        try{
          $this->db->beginTransaction();
            
            for($i = 0; $i < sizeof($tags); $i++){
                if(!empty($tags[$i])){
                $insertionrelation = $this->db->prepare('INSERT INTO 2etexte_contient_tag(id_contient_tag_extrait, id_contient_tag_tag) VALUES(:id_extrait, :id_tag)');
                $insertionrelation->execute(array('id_extrait' => $id_extrait,
                                                 'id_tag' => $tags[$i]));
                } else {
                    continue;
                }
            }
            
            $date_modif = date('Y-m-d H:i:s');
            $modif_extrait = $this->db->prepare('UPDATE 2etexte_extrait SET date_modif_extrait = :datemodif WHERE id_extrait = :extrait');
            $modif_extrait->execute(array('datemodif' => $date_modif, 
                                         'extrait' => $id_extrait));
            $this->db->commit();
        }
        catch(Exception $e){
            $this->db->rollBack();
            echo "<p>Echec: ".$e->getMessage()."</p>";
        }
        
        
    }
    
    public function insertNewTagInExtrait($tags, $id_extrait){
        
        date_default_timezone_set('Europe/Paris');
        
        try{
          $this->db->beginTransaction();
            
            for($i = 0; $i < sizeof($tags); $i++){
                if(!empty($tags[$i])){
                    $insertiontag = $this->db->prepare('INSERT INTO 2etexte_tag(nom_tag, type_tag) VALUES(:nomtag, :typetag)');
            $insertiontag->execute(array('nomtag' => $tags[$i]->nom_tag,
                                        'typetag' => $tags[$i]->type_tag));
                $lastid = $this->db->lastInsertId();
                $insertionrelation = $this->db->prepare('INSERT INTO 2etexte_contient_tag(id_contient_tag_extrait, id_contient_tag_tag) VALUES(:id_extrait, :id_tag)');
                $insertionrelation->execute(array('id_extrait' => $id_extrait,
                                                 'id_tag' => $lastid));
                } else {
                    continue;
                }
            }
            
            $date_modif = date('Y-m-d H:i:s');
            $modif_extrait = $this->db->prepare('UPDATE 2etexte_extrait SET date_modif_extrait = :datemodif WHERE id_extrait = :extrait');
            $modif_extrait->execute(array('datemodif' => $date_modif, 
                                         'extrait' => $id_extrait));
            $this->db->commit();
        }
        catch(Exception $e){
            $this->db->rollBack();
            echo "<p>Echec: ".$e->getMessage()."</p>";
        }
    }
}
?>