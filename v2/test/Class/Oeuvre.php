<?php
class Oeuvre{
    public $id_oeuvre;
    public $id_autrice_oeuvre;
    public $autrice_oeuvre;
    public $annee_oeuvre;
    public $reference_oeuvre;
    public $source_oeuvre;
    public $achat_oeuvre;
    
    public function __construct(array $donnees){
        foreach($donnees as $key=>$value){
            $this->$key = $value;
        }
    }
}
?>