<?php
include('../../parameters.php');

if(isset($_GET['t']) && $_GET['t'] !== ""){
    $optiontag = intval($_GET['t']);
} else {
    $optiontag = "";
}

if(isset($optiontag) && $optiontag !== ""){
    $recherche = $bdd->prepare('SELECT id_tag, type_tag, nom_tag, COUNT(id_extrait) AS nbre_extraits FROM 2etexte_tag, 2etexte_contient_tag, 2etexte_extrait WHERE type_tag = :type AND id_tag = id_contient_tag_tag AND id_extrait = id_contient_tag_extrait GROUP BY id_tag');
    $recherche->execute(array('type' => $optiontag));
    
    $min_nbextraits = INF;
    $max_nbextraits = -INF;
    
    while($donnees = $recherche->fetch()){
        $min_nbextraits = min($min_nbextraits, $donnees['nbre_extraits']);
        $max_nbextraits = max($max_nbextraits, $donnees['nbre_extraits']);
    }
    
    $recherche->execute(array('type' => $optiontag));
    
    while($donnees = $recherche->fetch()){
        $idtag = $donnees['id_tag'];
        
        $classe = "tag".$donnees['type_tag'];
        
        $taille = (1 + log($donnees['nbre_extraits'])) - log($min_nbextraits)/(1 + log($max_nbextraits)-log($min_nbextraits)) * 0.25;
        
        
        
            echo "<span style=\"font-size: ".$taille."rem;\" class=\"".$classe."\" tabindex=\"".$donnees['id_tag']."\">".$donnees['nom_tag']."</span> ";
    }
}

?>