<?php
include('../../parameters.php');

$id_autrice = intval(strip_tags($_GET['id_a']));

if(isset($_GET['cherche_a']) && !empty($_GET['cherche_a'])){
    $cherche_autrice = strip_tags($_GET['cherche_a']);
} else {
    $cherche_autrice = "";
}

if(isset($_GET['a']) && !empty($_GET['a']) && isset($_GET['o']) && !empty($_GET['o'])){
    $auteur = strip_tags($_GET['a']);
    $oeuvre = strip_tags($_GET['o']);
    
    $auteur = str_replace(['*', '+', '?', '(', ')', '|', '^', '$'], '', $auteur);
    $oeuvre = str_replace(['*', '+', '?', '(', ')', '|', '^', '$'], '', $oeuvre);
    
} else if(isset($_GET['o']) && !empty($_GET['o'])){
    $auteur = "";
    $oeuvre = strip_tags($_GET['o']);
    
    $oeuvre = str_replace(['*', '+', '?', '(', ')', '|', '^', '$'], '', $oeuvre);
}

if($id_autrice !== -1 && !empty($oeuvre)){
    $recherche = $bdd->query('SELECT * FROM 2etexte_oeuvre WHERE 2etexte_oeuvre.reference_oeuvre REGEXP "^'.$oeuvre.'" AND 2etexte_oeuvre.id_autrice_oeuvre = '.$id_autrice.'');
    
    while($donnees = $recherche->fetch()){
        echo "<p class=\"titre_oeuvre\">".$donnees['reference_oeuvre']."</p><br><br>";
        echo "<p class=\"idoeuvre\" style=\"display: none;\">".$donnees['id_oeuvre']."</p>";
    }
} else if(!empty($cherche_autrice) && !empty($oeuvre)) {
    
    echo "";
    
} else if(!empty($oeuvre)){
    $recherche = $bdd->query('SELECT * FROM 2etexte_oeuvre WHERE 2etexte_oeuvre.reference_oeuvre REGEXP "^'.$oeuvre.'"');
    
    while($donnees = $recherche->fetch()){
        echo "<p class=\"titre_oeuvre\">".$donnees['reference_oeuvre']."</p><br><br>";
        echo "<p class=\"auteur_oeuvre\" style=\"display: none;\">".$donnees['autrice_oeuvre']."</p>";
        echo "<p class=\"idoeuvre\" style=\"display: none;\">".$donnees['id_oeuvre']."</p>";
        echo "<p class=\"idaut\" style=\"display: none;\">".$donnees['id_autrice_oeuvre']."</p>";
    }
}
?>