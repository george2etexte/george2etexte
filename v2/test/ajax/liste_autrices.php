<?php
    include("../../parameters.php");

if(isset($_GET['r']) && !empty($_GET['r'])){
    $recherche = strip_tags($_GET['r']);
    $recherche = str_replace(['*', '+', '?', '(', ')', '|', '^', '$'], '', $recherche);
    
    $tablecaracteres = ['a', 'e', 'i', 'o', 'u', 'y', 'n', 'c', "'", '"'];
        $remplacement = ['[Aaäâà]', '[éEeèêë]', '[Iiîï]', '[Ooôö]', '[Uuûü]', '[Yyÿ]', '[Nnñ]', '[Ccç]', '(&lsquo;|&rsquo;|&ldquo;|&rdquo;)', '(&quot;|&laquo;&nbsp;|&raquo;&nbsp;)'];
    
    $recherche = trim(str_replace($tablecaracteres, $remplacement, $recherche));
    
} else {
    $recherche = "";
}

if(!empty($recherche)){
    $cherchons = $bdd->query('SELECT * FROM 2etexte_autrice WHERE prenom_autrice REGEXP "^'.$recherche.'" OR nom_autrice REGEXP "^'.$recherche.'" OR REPLACE(prenom_autrice, " ", "") REGEXP "^'.$recherche.'" OR REPLACE(nom_autrice, " ", "") REGEXP "^'.$recherche.'" OR CONCAT(prenom_autrice, " ", nom_autrice) REGEXP "^'.$recherche.'" OR CONCAT(prenom_autrice, "", nom_autrice) REGEXP "^'.$recherche.'" OR CONCAT(nom_autrice, " ", prenom_autrice) REGEXP "^'.$recherche.'" OR CONCAT(nom_autrice, "", prenom_autrice) REGEXP "^'.$recherche.'" OR CONCAT(REPLACE(prenom_autrice, " ", ""), " ", nom_autrice) REGEXP "^'.$recherche.'" OR CONCAT(REPLACE(prenom_autrice, " ", ""), "", nom_autrice) REGEXP "^'.$recherche.'" OR CONCAT(REPLACE(nom_autrice, " ", ""), " ", prenom_autrice) REGEXP "^'.$recherche.'" OR CONCAT(REPLACE(nom_autrice, " ", ""), "", prenom_autrice) REGEXP "^'.$recherche.'" OR CONCAT(prenom_autrice, " ", REPLACE(nom_autrice, " ", "")) REGEXP "^'.$recherche.'" OR CONCAT(prenom_autrice, "", REPLACE(nom_autrice, " ", "")) REGEXP "^'.$recherche.'" OR CONCAT(nom_autrice, " ", REPLACE(prenom_autrice, " ", "")) REGEXP "^'.$recherche.'" OR CONCAT(nom_autrice, "", REPLACE(prenom_autrice, " ", "")) REGEXP "^'.$recherche.'"');
    
    while($donnees = $cherchons->fetch()){
        
            echo "<p class=\"nom_autrice\">".$donnees['prenom_autrice']." ".$donnees['nom_autrice']."</p><br><br>";
        echo "<p style=\"display: none;\" class=\"idautrice\">".$donnees['id_autrice']."</p>";
            
}
    
}
?>