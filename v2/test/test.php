<?php

header('Content-type: xml');

function chargerClasse($classe){
    require '../Class/'.$classe.'.php';
}

spl_autoload_register('chargerClasse');

if(isset($_GET['record']) && !empty($_GET['record'])){
    $record = intval($_GET['record']);
}

if(isset($_GET['auteur']) && !empty($_GET['auteur'])){
    $auteur = $_GET['auteur'];
}

if(isset($_GET['oeuvre']) && !empty($_GET['oeuvre'])){
    $oeuvre = $_GET['oeuvre'];
}



$xml = file_get_contents('http://catalogue.bnf.fr/api/SRU?version=1.2&operation=searchRetrieve&query=bib.author%20all%20%22'.urlencode($auteur).'%22%20and%20bib.title%20all%20%22'.urlencode($oeuvre).'%22&recordSchema=unimarcxchange&maximumRecords=20&startRecord=1');




if($xml){
    echo $xml;
}
    ?>