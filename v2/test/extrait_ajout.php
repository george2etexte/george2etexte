<html>
<head>
   <title>George, le deuxième texte</title>
   <meta http-equiv="Content-Type" content="text/html; charset=UTF8" />
   <link rel="stylesheet" href="../style.css" />
   <link href="../dist/css/bootstrap.css" rel="stylesheet">
   <link rel="shortcut icon" href="../images/icone_george2etexte.ico" type="images/x-icon" />  
</head>

<body style="background-color:white;font-size:12pt;">
<SCRIPT TYPE="text/javascript" SRC="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></SCRIPT>
<script src="https://cdn.ckeditor.com/4.7.1/standard/ckeditor.js"></script>
<!--<script src="./js/ckeditor/ckeditor.js"></script>-->
<SCRIPT>
$(document).ready(function(){
   $("h2").hide();
})
</SCRIPT>

<?php
include("./header.php");
?>

<!--<hr/>-->

<div  style="background-color:#F5F5F5;margin-top:20px;padding:20px;">

<div class="container">



<?php
include("../parameters.php");
include("../functions.php");
?>



<div class="panel panel-default" style="text-align:center;padding:20px;">
  <div class="panel-body">
  <h1 class="form-signin-heading">Ajout d'un extrait</h1>
  <hr/>
  <form action="extrait_ajout_ok.php" method="post">
  
  <div class="panel-body" style="background-color:#F1EEFF;">
  <b>Cette interface est mise en place à titre expérimental.</b><br/>
  En attendant qu'elle propose des comptes personnels, merci d'indiquer ci-dessous 
  qui vous êtes parmi nos utilisatrices et utilisateurs :<br/>
  <select name="utilisatrice_extrait" class="form-control">
  <option value="-1" style="background-color:#FF8888;">Je n'apparais pas dans la liste</option>
  <?php
  $sql = 'SELECT * FROM 2etexte_utilisatrice WHERE 1 ORDER BY 2etexte_utilisatrice.nom_utilisatrice';
  $req = mysqli_query($link, $sql)
  or die('Erreur SQL !<br>'.$sql.'<br>'.mysqli_error($link));
  while($data = mysqli_fetch_assoc($req)){
     echo "<option value=\"".$data["id_utilisatrice"]."\">".$data["nom_utilisatrice"]." ".$data["prenom_utilisatrice"]."</option>";
  }
  ?>
  </select>
  <i>Si votre nom n'apparaît pas dans la liste, merci de nous envoyer un mail à l'adresse <a href="mailto:george-2e-texte@gmx.fr">george-2e-texte@gmx.fr</a>
  en nous transmettant votre prénom, votre nom, quelques mots de présentation <small>(par exemple : Prof de lettres au lycée Claude-Nicolas-Ledoux de Besançon)</small>,
  une image, si possible au format carré faisant au moins 150 pixels de côté, ainsi qu'une adresse de site web, éventuellement.</i>
  </div>
  
  <br/><br/>
  &OElig;uvre d'où est tiré l'extrait :<br/>
  <select name="oeuvre_extrait" class="form-control">
  <option value="-1" style="background-color:#FF8888;">&OElig;uvre non présente dans la base de données</option>
  <?php
  $sql = 'SELECT * FROM 2etexte_oeuvre,2etexte_autrice WHERE 2etexte_oeuvre.id_autrice_oeuvre=2etexte_autrice.id_autrice ORDER BY 2etexte_autrice.nom_autrice';
  $req = mysqli_query($link, $sql)
  or die('Erreur SQL !<br>'.$sql.'<br>'.mysqli_error($link));
  while($data = mysqli_fetch_assoc($req)){
     if(strlen($data["reference_oeuvre"])+strlen($data["nom_autrice"])+strlen($data["prenom_autrice"])+3>120){
        $add="...";
     }else{
        $add="";
     }
     echo "<option value=\"".$data["id_oeuvre"]."\">".fullName($data["nom_autrice"],$data["prenom_autrice"])." - ".mb_substr($data["reference_oeuvre"],0,120-strlen($data["nom_autrice"])-strlen($data["prenom_autrice"])-5).$add."</option>";
  }
  ?>
  </select>
  <br/><br/>
  Titre de l'extrait :<br/>
  <input type="text" name="titre_extrait" class="form-control">
  <br/><br/>
  Introduction (facultative), courte présentation de l'extrait, destinée aux enseignantes et enseignants qui l'utiliseront :<br/>
  <textarea name="intro_extrait"></textarea><br/>
  Sous quelle licence de diffusion souhaitez-vous mettre à disposition le texte d'introduction ci-dessus ? 
  <select name="droits_intro_extrait" class="form-control">
  <option value="by-sa" selected>Creative Commons CC BY-SA 4.0 (attribution, partage dans les mêmes conditions)</option>
  <option value="by-nc-sa">Creative Commons CC BY-NC-SA 4.0 (attribution, partage dans les mêmes conditions, pas d'utilisation commerciale)</option>
  </select>
  <br/><br/>
  Extrait :<br/>
  <textarea name="texte_extrait"></textarea>
  <br/>
  Source de l'extrait (lien éventuel vers une page web où se trouve cet extrait) :<br/>
  <input type="text" name="source_extrait" class="form-control">
  <br/><br/>
  <!--
  Autrice ou auteur :<br/>
  <select name="oeuvre">
  <?php
  $sql = 'SELECT * FROM 2etexte_autrice WHERE 1 ORDER BY 2etexte_autrice.nom_autrice';
  $req = mysqli_query($link, $sql)
  or die('Erreur SQL !<br>'.$sql.'<br>'.mysqli_error($link));
  while($data = mysqli_fetch_assoc($req)){     
     echo "<option value=\"".$data["id_autrice"]."\">".$data["nom_autrice"].", ".$data["prenom_autrice"]."</option>";
  }
  ?>
  </select>
  -->
  <br/><br/>
  <input type="submit" value="Envoyer">
  <script>
  CKEDITOR.replace( 'intro_extrait' );
  CKEDITOR.replace( 'texte_extrait' );
  </script>
  </form>
  </div>
</div>

  
<?php
include("./footer.php");
?> 
</body>
</html>
