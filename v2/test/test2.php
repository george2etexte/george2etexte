<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Test 2</title>
</head>
<body>
    <div class="contenu" style="display: none;"></div>
   <div class="liste">
       <ul id="listing">
           
       </ul>
   </div>
    <script src="../js/jquery-3.2.1.min.js"></script>
    <script>
        function replaceAll(code, searchStr, replaceStr){
            while(code.indexOf(searchStr) > -1){
                code = code.replace(searchStr, replaceStr);
            }
            return code;
        }
        
        function repeatAjax(record, ajout, oeuvre){
            $.ajax({
               url: 'http://localhost/george2etexte-master-41d9707b5273f192306e66f4caa2e8832f2be71b/george2etexte-master-41d9707b5273f192306e66f4caa2e8832f2be71b/v2/test/test.php',
                method: 'GET',
                data: {ajout: ajout, record: record, oeuvre: encodeURI(oeuvre)},
                dataType: 'html',
                success: function(code, statut){
                    
                    if(code){
                        codeur = replaceAll(code, "mxc:datafield", "mxcdatafield");
                        codeur = replaceAll(codeur, "mxc:subfield", "mxcsubfield");
                        $('.contenu').html("");
                $('.contenu').html(codeur);
                    
                
                    var nouveauTableau = [];
                        
                        if($('mxcdatafield[tag="200"]>mxcsubfield[code="a"]')[0]){
                            for(var i = 0; i < $('mxcdatafield[tag="200"]>mxcsubfield[code="a"]').length; i++){
                        
                        var texteTableau = $('mxcdatafield[tag="200"]>mxcsubfield[code="a"]')[i].textContent.replace('[', '').replace(']', '');
                        
                        if(nouveauTableau.indexOf(texteTableau) !== -1){
                            nouveauTableau[i] = "";
                        } else {
                            nouveauTableau[i] = texteTableau;
                        }
                        
                    }
                    
                     
                    for(var j = 0; j < nouveauTableau.length; j++){
                        
                        if(nouveauTableau[j] !== "" && $('#listing li').text() !== nouveauTableau[j]){
                            $('#listing').html($('#listing').html() + "<li>"+ nouveauTableau[j] +"</li>");
                        }
                    }
                        } else {
                            $('#listing').html("<p>Aucun résultat</p>");
                        }
                    }
                        
                      
                }
            });
        }
        
        
        $(document).ready(function(){
             var record = 1, ajout = 20;
        var oeuvre = "<?php echo strip_tags($_GET['oeuvre']); ?>";
            
            repeatAjax(record, ajout, oeuvre);
            
        });
    </script>
</body>
</html>