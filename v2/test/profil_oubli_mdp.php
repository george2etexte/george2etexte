<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../dist/css/bootstrap.css">
    <link rel="stylesheet" href="../dist/css/normalize.css">
    <link rel="stylesheet" href="../style.css">
    <title>Oubli de mot de passe</title>
</head>
<body>
   <a href="profil_connex_insc.php">Retour</a>
    <form action="profil_oubli_envoi_mail.php" method="post">
        <fieldset><legend>Mot de passe oublié ?</legend>
            <label for="prenom">Prénom</label><input type="text" name="prenom" id="prenom" class="form-control-static" required autofocus><br><br>
            <label for="nom">Nom</label><input type="text" name="nom" id="nom" class="form-control-static" required><br><br>
            <label for="mail">Adresse mail</label><input type="email" name="mail" id="mail" class="form-control-static" required><br><br><div id="nomail"></div><br><br>
            <input type="submit" value="Envoi d'un mail">
        </fieldset>
    </form>
    <script src="../js/jquery-3.2.1.min.js"></script>
    <script>
        function insertAjax(){
            var param = "m=" + $('#mail').val() + '&p=' + $('#prenom').val() + '&n=' + $('#nom').val();
            var envoi = document.querySelector('input[type="submit"]');
               $.ajax({
                  url: 'ajax/no_correspondance.php',
                   type: 'GET',
                   data: param,
                   success: function(code, statut){
                       if(code == ""){
                $('#nomail').children('p').replaceWith("");
                           envoi.disabled = false;
            } else {
                $('#nomail').children('p').replaceWith("");
                $(code).appendTo('#nomail');
                envoi.disabled = true;
            }
                   }
               });
        }
    $(document).ready(function(){
           $('#mail').blur(insertAjax);
        $('#prenom').blur(insertAjax);
        $('#nom').blur(insertAjax);
    })
    </script>
</body>
</html>