<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../dist/css/bootstrap.css">
    <link rel="stylesheet" href="../dist/css/normalize.css">
    <link rel="stylesheet" href="../style.css">
    <title>Envoi mail</title>
</head>
<body>
    <?php
    function chargerClasse($classe){
        require "Class/".$classe.".php";
    }
    spl_autoload_register('chargerClasse');
    include("../parameters.php");
    
    
    $manager = new UtilisatriceManager($bdd);
    $identifiant = $manager->getUtilisatriceByMail($_POST['mail']);
    if($identifiant !== 0){
        $utilisatrice = $manager->getUtilisatriceById($identifiant);
        $prenom = $utilisatrice->getPrenom_utilisatrice();
        $nom = $utilisatrice->getNom_utilisatrice();
        if($_POST['prenom'] == $prenom && $_POST['nom'] == $nom){
            
            $token = hash("sha256", $utilisatrice->getId_utilisatrice() . $salage);
            $util_token = $utilisatrice->getToken_utilisatrice();
            
            $verif = $manager->getUtilisatriceByToken($token);
                if($util_token !== $token && $verif == 0){
                $manager->updateToken($identifiant, $token);
            }
            
            
     $headers  = 'MIME-Version: 1.0' . "\r\n";
     $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
            mail($_POST['mail'], "Oubli de mot de passe de George le 2ème texte", "<p>Voici le lien pour changer de mot de passe</p><a href=\"http://localhost/george2etexte-master-41d9707b5273f192306e66f4caa2e8832f2be71b/george2etexte-master-41d9707b5273f192306e66f4caa2e8832f2be71b/v2/test/oubli_mdp/form_mdp.php?id=".$token."\">Modifier le mot de passe</a>", $headers);
            echo "<p>Mail envoyé</p>";
        } else {
            echo "<p>Votre prénom et votre nom ne correspondent pas au mail renseigné</p>";
        }
    } else {
        echo "<p>Le mail n'existe pas dans la base de données</p>";
    }
    ?>
    <a href="profil_oubli_mdp.php">Retour</a>
</body>
</html>