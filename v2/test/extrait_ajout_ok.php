<html>
<head>
   <title>George, le deuxième texte</title>
   <meta http-equiv="Content-Type" content="text/html; charset=UTF8" />
   <link rel="stylesheet" href="../style.css" />
   <link href="../dist/css/bootstrap.css" rel="stylesheet">
   <link rel="shortcut icon" href="../images/icone_george2etexte.ico" type="images/x-icon" />  
</head>

<body style="background-color:white;font-size:12pt;">
<SCRIPT TYPE="text/javascript" SRC="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></SCRIPT>
<SCRIPT>
$(document).ready(function(){
   $("h2").hide();
})
</SCRIPT>

<?php
include("./header.php");
?>

<!--<hr/>-->

<div  style="background-color:#F5F5F5;margin-top:20px;padding:20px;">

<div class="container">



<?php
include("../parameters.php");
include("../functions.php");

if (isset($_POST["titre_extrait"])) {
    $titre_extrait = stringClean($_POST["titre_extrait"]);
} else {
    $titre_extrait = "";
}

if (isset($_POST["texte_extrait"])) {
    $texte_extrait = stringClean($_POST["texte_extrait"]);
} else {
    $texte_extrait = "";
}

if (isset($_POST["intro_extrait"])) {
    $intro_extrait = stringClean($_POST["intro_extrait"]);
} else {
    $intro_extrait = "";
}

if (isset($_POST["droits_intro_extrait"])) {
    $droits_intro_extrait = stringClean($_POST["droits_intro_extrait"]);
} else {
    $droits_intro_extrait = "";
}

if (isset($_POST["source_extrait"])) {
    $source_extrait = stringClean($_POST["source_extrait"]);
} else {
    $source_extrait = "-";
}

if (isset($_POST["utilisatrice_extrait"])) {
    $utilisatrice_extrait = intval($_POST["utilisatrice_extrait"]);
} else {
    $utilisatrice_extrait = "";
}

if (isset($_POST["oeuvre_extrait"])) {
    $oeuvre_extrait = intval($_POST["oeuvre_extrait"]);
} else {
    $oeuvre_extrait = "";
}

//$extrait = strip_tags($extrait,"<p><b><i><u><h1><h2><h3><h4><h5>");

$sql = "INSERT INTO 2etexte_extrait (titre_extrait,texte_extrait,intro_extrait,droits_intro_extrait,source_extrait,utilisatrice_extrait,oeuvre_extrait,date_creation_extrait,date_modif_extrait)
VALUES ('".$titre_extrait."','".$texte_extrait."','".$intro_extrait."','".$droits_intro_extrait."','".$source_extrait."',".$utilisatrice_extrait.",".$oeuvre_extrait.",'".date("Y-m-d H:i:s")."','".date("Y-m-d H:i:s")."');";

/*
$sql = 'SELECT * FROM 2etexte_utilisatrice,2etexte_extrait,2etexte_contient_extraits,2etexte_oeuvre,2etexte_autrice WHERE 2etexte_utilisatrice.id_utilisatrice=2etexte_extrait.utilisatrice_extrait AND 2etexte_extrait.oeuvre_extrait=2etexte_oeuvre.id_oeuvre AND 2etexte_oeuvre.id_autrice_oeuvre=2etexte_autrice.id_autrice AND 2etexte_extrait.id_extrait='.$id_extrait;
$req = mysqli_query($link, $sql)
or die('Erreur SQL !<br>'.$sql.'<br>'.mysqli_error($link));
$data = mysqli_fetch_assoc($req);
*/
?>

<div class="panel panel-default" style="text-align:center;padding:20px;">
  <div class="panel-body">
  <h1 class="form-signin-heading">Ajout d'un extrait</h1>
  <hr/>
   <?php
   /*var_dump($_POST);
   echo "<hr/>";
   echo "<tt>".$sql."</tt>";
   */
   
   
   ///*
   $req = mysqli_query($link, $sql) or die('Erreur SQL ! Malheureusement votre extrait n&rsquo;a pas pu être ajouté,
   merci de contacter george-2e-texte@gmx.fr à ce sujet.');//.mysqli_error($link));
   echo "Merci beaucoup pour votre contribution, qui a été ajoutée à notre base de données.";
   echo "<br/><br/>Retour à <a href=\"extrait_ajout.php\">la page d'ajout d'extraits</a>, à l'<a href=\"index.php\">accueil</a>."
   //*/

   ?>
  </div>
</div>

  
<?php
include("./footer.php");
?> 
</body>
</html>
