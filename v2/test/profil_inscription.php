<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../dist/css/normalize.css">
    <link rel="stylesheet" href="../dist/css/bootstrap.css">
    <link rel="stylesheet" href="../style.css">
    <title>Inscription</title>
</head>
<body>
    <?php
    function chargerClasse($classe){
        require "Class/".$classe.".php";
    }
    spl_autoload_register('chargerClasse');
    include("../parameters.php");
    $prenom = strip_tags(htmlspecialchars($_POST['prenom']));
    $nom = strip_tags(htmlspecialchars($_POST['nom']));
    $mail = strip_tags(htmlspecialchars($_POST['mailing']));
    if(isset($_POST['presentation'])){
        $presentation = strip_tags(htmlspecialchars($_POST['presentation']));
    }
    
    if(isset($_POST['lien_site'])){
        $lien = strip_tags(htmlspecialchars($_POST['lien_site']));
    }
    
    if(isset($_POST['institution'])){
        $institution = strip_tags(htmlspecialchars($_POST['institution']));
    }
    
    if(isset($_POST['etablissement'])){
        $etablissement = strip_tags(htmlspecialchars($_POST['etablissement']));
    }
    
    if(isset($_POST['academie'])){
        $academie = strip_tags(htmlspecialchars($_POST['academie']));
    }
    
    
    $mdp = strip_tags(htmlspecialchars($_POST['mdpin']));
    $confirmation = strip_tags(htmlspecialchars($_POST['confirmation']));
    
    $utilisatriceinsert = new UtilisatriceManager($bdd);
    
    if($utilisatriceinsert->getUtilisatriceByMail($mail) == 0){
        if($utilisatriceinsert->getUtilisatriceByWebsite($lien) == 0){
            if($mdp == $confirmation){
                
                if(isset($_POST['avatar']) && !empty($_POST['avatar'])){
                    $hashmail = md5(strtolower(trim($mail)));
                    $adresse = 'https://www.gravatar.com/avatar/'. $hashmail .'?s=200&r=g&d=wavatar';
                } else if(isset($_FILES['photo']['name']) && !empty($_FILES['photo']['name'])){
        $contenu = '../photos/';
        $tmp_name = $_FILES['photo']['tmp_name'];
        if(is_uploaded_file($tmp_name)){
            $tableau_fichier = explode(".", $_FILES['photo']['name']);
            $nouveau_nom = hash("sha256", $tableau_fichier[0] . rand()) . "." . $tableau_fichier[1];
            $adresse_photo = $contenu . $nouveau_nom;
            move_uploaded_file($tmp_name, $adresse_photo);
            $adresse = 'photos/' . $nouveau_nom;
        }
    } else {
        $adresse = 'photos/anonyme.png';
    }
                $hashage = password_hash($mdp . $salage, PASSWORD_DEFAULT);
                
        if(password_verify($mdp . $salage, $hashage)){
            $utilisatrice = new Utilisatrice(array('prenom_utilisatrice' => $prenom,
                                          'nom_utilisatrice' => $nom,
                                          'mail_utilisatrice' => $mail,
                                          'presentation_utilisatrice' => $presentation,
                                          'lien_utilisatrice' => $lien,
                                          'photo_utilisatrice' => $adresse,
                                          'adresse_institutionnelle' => $institution,
                                          'etablissement' => $etablissement,
                                          'academie' => $academie,
                                          'mdp_utilisatrice' => $hashage));
        $utilisatriceinsert->insertUtilisatrice($utilisatrice);
        echo "<p>Vous êtes inscrit(e) !</p>";
        }
    } else {
        echo "<p>Votre mot de passe de confirmation n'est pas bon par rapport au mot de passe que vous avez tapé. Recommencez.</p>";
    }
        } else {
            echo "<p>Le site web est déjà la possession de quelqu'un d'autre.</p>";
        }
    } else {
        echo "<p>Ce mail existe déjà !</p>";
    }
    
    ?>
    <a href="profil_connex_insc.php">Retour</a>
</body>
</html>