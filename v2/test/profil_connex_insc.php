<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../dist/css/bootstrap.css">
    <link rel="stylesheet" href="../dist/css/normalize.css">
    <link rel="stylesheet" href="../style.css">
    <title>Connexion | Inscription</title>
</head>
<body>
  <?php
    include('../header.php');
    ?><br>
    <a href="../index.php">Retour</a>
   <fieldset class="panel panel-body"><legend>Connexion à votre compte</legend>
       <form action="profil_connexion.php" method="post">
        <label for="mail">Adresse mail</label><br><input type="email" name="mail" id="mail" class="form-control-static" required autofocus><br><br><div id="pasdemail"></div><br>
        <label for="mdp">Mot de passe</label><br><input type="password" name="mdp" id="mdp" class="form-control-static" required><br><br><div id="pasdemdp"></div><br>
        <input type="submit" value="Se connecter" id="envoiconnexion" class="form-control-static btn btn-default"><br><br>
    </form>
    <a href="profil_oubli_mdp.php">Mot de passe oublié ?</a>
   </fieldset>
   <fieldset class="panel panel-body"><legend>Vous n'avez pas encore de compte ? Inscrivez-vous !</legend>
       <form action="profil_inscription.php" method="post" id="form2" enctype="multipart/form-data">
           <label for="prenom">Prénom</label><br><input type="text" name="prenom" id="prenom" class="form-control-static" required><br><br>
           <label for="nom">Nom</label><br><input type="text" name="nom" id="nom" class="form-control-static" required><br><br>
           <label for="mailing">Adresse mail</label><br><input type="email" name="mailing" id="mailing" class="form-control-static" required><br><br><div id="nomail"></div><div id="prev"></div>
           <label for="presentation">Présentez-vous !</label><br><br>
           <textarea name="presentation" id="presentation" cols="30" rows="10" class="form-control" placeholder="Présentez-vous brièvement..." maxlength="500"></textarea><br><br><span id="ecrit">500 caractères restants</span><br><br>
           <label for="lien_site">Lien vers votre site web</label><br><input type="url" name="lien_site" id="lien_site" placeholder="Au début, http:// ou https://" class="form-control-static"><br><br><div id="nolien"></div>
           <label for="etablissement">&Eacute;tablissement</label><br><input type="text" name="etablissement" id="etablissement" class="form-control-static"><br><br>
           <strong>Académie</strong><br>
           <select name="academie" id="academie" class="form-control-static">
               <option value="">-- Choisissez votre académie --</option>
           </select><br><br>
           <label for="institution">Adresse de courriel institutionnel</label><br><input type="email" name="institution" id="institution" class="form-control-static"><br><br>
           <label for="mdpin">Mot de passe (minimum 8 caractères)</label><br><input type="password" name="mdpin" id="mdpin" class="form-control-static" required pattern="^.{8,}$"><br><br>
           <label for="confirmation">Confirmez votre mot de passe</label><br><input type="password" name="confirmation" id="confirmation" class="form-control-static" required pattern="^.{8,}$"><br><br><div id="noconfirmation"></div>
           <p class="profil">Choisissez une photo pour votre profil:
           <input type="file" name="photo" id="photo" accept="image/jpeg">
           </p><br>
           <p>Ou vous pouvez générer un avatar aléatoire: </p>
           <input type="checkbox" name="avatar" id="avatar" value="OK !"> <label for="avatar">Je veux avoir un avatar aléatoire</label><br><br>
           <div id="avatar_apparition"></div><br>
           <input type="submit" value="S'inscrire" id="envoiinscription" class="form-control-static btn btn-default"><br><br>
           <input type="reset" value="Recommencer" id="resume" class="form-control-static btn btn-default">
       </form>
   </fieldset>
   <script>
       var ecrit = document.querySelector('#ecrit');
       var textarea = document.getElementById('presentation');
       textarea.addEventListener('keyup', function(){
           var caracteres = 500 - textarea.value.length;
           if(caracteres <= 1){
               ecrit.textContent = caracteres + " caractère restant";
           } else {
               ecrit.textContent = caracteres + " caractères restants";
           }
       });
    </script>
   <script src="../js/jquery-3.2.1.min.js"></script>
   <script>
    var academie = document.getElementById('academie'),
        institution = document.getElementById('institution'),
listeacademies = {"Aix-Marseille":"ac-aix-marseille.fr",
                  "Amiens":"ac-amiens.fr",
                  "Besançon":"ac-besancon.fr",
    "Bordeaux":"ac-bordeaux.fr",
    "Caen":"ac-caen.fr",
"Clermont-Ferrand":"ac-clermont.fr",
"Corse":"ac-corse.fr",
"Créteil":"ac-creteil.fr",
"Dijon":"ac-dijon.fr",
"Grenoble":"ac-grenoble.fr",
"Guadeloupe":"ac-guadeloupe.fr",
"Guyane":"ac-guyane.fr",
"La Réunion":"ac-reunion.fr",
"Lille":"ac-lille.fr",
"Limoges":"ac-limoges.fr",
"Lyon":"ac-lyon.fr",
"Martinique":"ac-martinique.fr",
"Mayotte":"ac-mayotte.fr",
"Montpellier":"ac-montpellier.fr",
"Nancy-Metz":"ac-nancy-metz.fr",
"Nantes":"ac-nantes.fr",
"Nice":"ac-nice.fr",
"Nouvelle-Calédonie":"ac-noumea.nc",
"Orléans-Tours":"ac-orleans-tours.fr",
"Paris":"ac-paris.fr",
"Poitiers":"ac-poitiers.fr",
"Polynésie Française":"monvr.pf",
"Reims":"ac-reims.fr",
"Rennes":"ac-rennes.fr",
"Rouen":"ac-rouen.fr",
"Strasbourg":"ac-strasbourg.fr",
"Toulouse":"ac-toulouse.fr",
"Versailles":"ac-versailles.fr",
"Wallis et Futuna":"ac-wf.wf"};
       
       for(var property in listeacademies){
           var option = document.createElement('option');
           option.textContent = property;
           option.value = property;
           academie.appendChild(option);
       }
       
       institution.addEventListener('blur', function(){
          var valeurinstitution = institution.value;
           var tableausepare = valeurinstitution.split("@");
           
           for(var prop in listeacademies){
               if(listeacademies[prop] == tableausepare[1]){
                   var optionselect = document.querySelector('#academie option[value="'+ prop +'"]');
                   optionselect.selected = true;
                   break;
               } else {
                   academie.options[0].selected = true;
               }
           }
       });
       
       academie.addEventListener('change', function(){
           var valeurac = academie.value;
           if(valeurac !== ""){
               if(institution.value !== ""){
                   var tableausepare = institution.value.split("@");
                   institution.value = tableausepare[0] + "@" + listeacademies[valeurac];
               } else {
                   institution.value = "@" + listeacademies[valeurac];
               }
           } else {
               var tableausepare = institution.value.split("@");
               institution.value = tableausepare[0];
           }
       });
    </script>
    <script>
    var paragphoto = document.querySelector('.profil'),
    mdp1 = document.getElementById('mdpin'), 
        mdp2 = document.getElementById('confirmation'),
        mailing = document.getElementById('mailing'),
        envoi = document.getElementById('envoiinscription'),
        envoico = document.getElementById('envoiconnexion'),
        resume = document.getElementById('resume'),
        nomail = document.getElementById('nomail'),
        nolien = document.getElementById('nolien'),
        photo = document.getElementById('photo'),
        checkbox = document.getElementById('avatar'),
        prev = document.getElementById('prev'),
        noconfirmation = document.getElementById('noconfirmation'),
        pasdemail = document.getElementById('pasdemail'),
        pasdemdp = document.getElementById('pasdemdp'), 
        choix = document.getElementById('choix'),
        avatar = document.getElementById('avatar_apparition');
        
        function verifMdp(valeur1, valeur2){
            if(valeur1 !== valeur2 && valeur1 !== "" && valeur2 !== ""){
               noconfirmation.innerHTML = "<p>Le mot de passe de confirmation ne correspond pas au mot de passe précédemment tapé</p>";
            } else {
                noconfirmation.innerHTML = "";
            }
        }
        
        function verifInscription(){
            if(nomail.textContent !== "" || nolien.textContent !== "" || noconfirmation.textContent !== ""){
                           envoi.disabled = true;
                       } else {
                           envoi.disabled = false;
                       }
        }
        
        function verifConnexion(){
            if(pasdemdp.textContent !== "" || pasdemail.textContent !== ""){
                           envoico.disabled = true;
                       } else {
                           envoico.disabled = false;
                       }
        }
        
        function insertionAjax(div, code){
            if(code == ""){
                $(div).children('p').replaceWith("");
            } else {
                $(div).children('p').replaceWith("");
                $(code).appendTo(div);
            }
        }
        
        function effaceContenu(){
            var divs = document.querySelectorAll('#form2 div');
            for(var i = 0; i < divs.length; i++){
                $('#' + divs[i].id).children('p').replaceWith("");
                $('#' + divs[i].id).children('img').replaceWith("");
                $('#' + divs[i].id).children('figcaption').replaceWith("");
            }
            envoi.disabled = false;
        }
        
        mdp1.addEventListener('blur', function(e){
           verifMdp(mdp1.value, mdp2.value);
            verifInscription();
        });
        
        mdp2.addEventListener('blur', function(e){
           verifMdp(mdp1.value, mdp2.value);
            verifInscription();
        });
        
       resume.addEventListener('click', function(){
           effaceContenu();
       });
        
        checkbox.addEventListener('click', function(){
            if(checkbox.checked == true){
                paragphoto.innerHTML = "Choisissez une photo pour votre profil:";
                
                if(mailing.value.indexOf("@") !== -1 && mailing.value !== ""){
                 var envoi = 'e=' + mailing.value;
                 }
                $.ajax({
                   url: 'ajax/md5_mail.php',
                    method: 'GET',
                    data: envoi,
                    success: function(code, status){
                        if(avatar.innerHTML !== ""){
                            $(avatar).children('img').replaceWith("");
                            $(avatar).children('figcaption').replaceWith("");
                            $(code).appendTo(avatar);
                        } else {
                            $(code).appendTo(avatar);
                        }
                    }
                });
                
            } else {
                
                avatar.innerHTML = "";
                
                var photography = document.createElement('input');
                photography.type = "file";
                photography.name = "photo";
                photography.id = "photo";
                photography.accept = "image/jpeg";
                paragphoto.appendChild(photography);
            }
        });
        
        $(document).ready(function(){
           $('#mailing').blur(function(){
                var param = "m=" + $('#mailing').val();
               $.ajax({
                  url: 'ajax/no_mail.php',
                   type: 'GET',
                   data: param,
                   success: function(code, statut){
                       insertionAjax("#nomail", code);
                       verifInscription();
                   }
               });
    });
         $('#lien_site').blur(function(){
                var param2 = "s=" + $('#lien_site').val();
               $.ajax({
                  url: 'ajax/no_website.php',
                   type: 'GET',
                   data: param2,
                   success: function(code, statut){
                       insertionAjax("#nolien", code);
                       verifInscription();
                   }
               });
    });
            $('#mail').blur(function(){
           var param3 = "m=" + $('#mail').val();
                $.ajax({
                  url: 'ajax/no_mail_2.php',
                   type: 'GET',
                   data: param3,
                   success: function(code, statut){
                       insertionAjax("#pasdemail", code);
                       verifConnexion();
                   }
               });
       });
            
          $('#mdp').blur(function(){
           var param4 = "m=" + $('#mail').val();
           var param5 = "p=" + $('#mdp').val();
           var param6 = param4 + "&" + param5;
              $.ajax({
                  url: 'ajax/no_mdp.php',
                   type: 'GET',
                   data: param6,
                   success: function(code, statut){
                       insertionAjax("#pasdemdp", code);
                       verifConnexion();
                   }
               });
       });  
       });
        
    </script>
</body>
</html>