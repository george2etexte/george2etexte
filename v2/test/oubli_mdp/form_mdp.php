<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../../dist/css/normalize.css">
    <link rel="stylesheet" href="../../dist/css/bootstrap.css">
    <link rel="stylesheet" href="../../style.css">
    <title>Formulaire mot de passe</title>
</head>
<body>
    <?php
    function chargerClasse($classe){
        require "../Class/".$classe.".php";
    }
    spl_autoload_register('chargerClasse');
    include("../../parameters.php");
    
    if(isset($_GET['id'])){
        $manager = new UtilisatriceManager($bdd);
        $identifiant = $manager->getUtilisatriceByToken($_GET['id']);
        if($identifiant !== 0){
            ?>
            <form action="modif_mdp.php" method="post">
               <input type="hidden" name="token" id="token" value="<?php echo $_GET['id']; ?>">
                <label for="mdpin">Nouveau mot de passe</label><input type="password" name="mdpin" id="mdpin" class="form-control-static" required autofocus pattern="^.{8,}$"><br><br>
                <label for="confirmation">Confirmation de mot de passe</label><input type="password" name="confirmation" id="confirmation" class="form-cotrol-static" required pattern="^.{8,}$"><br><br><div id="problematique"></div><br><br>
                <input type="submit" value="Modifier le mot de passe" id="envoi">
            </form>
            <?php
        } else {
            echo "Désolé, nous ne pouvons pas modifier votre mot de passe. Nous n'avons pas les informations nécessaires pour vous identifier.";
        }
    } else {
        echo "Désolé, nous ne pouvons pas modifier votre mot de passe. Nous n'avons pas les informations nécessaires pour vous identifier.";
    }
    ?>
    <script>
    var text1 = document.getElementById('mdpin'),
        text2 = document.getElementById('confirmation'),
        probleme = document.getElementById('problematique'),
        envoi = document.getElementById('envoi');
        
        text1.addEventListener('blur', function(){
           var mdp1 = text1.value,
        mdp2 = text2.value;
            if(mdp1 !== mdp2 && mdp1 !== "" && mdp2 !== ""){
                probleme.textContent = "Le mot de passe de confirmation n'est pas bon";
                envoi.disabled = true;
            } else {
                probleme.textContent = "";
                envoi.disabled = false;
            }
        });
        
        text2.addEventListener('blur', function(){
            var mdp1 = text1.value,
        mdp2 = text2.value;
            if(mdp1 !== mdp2 && mdp1 !== "" && mdp2 !== ""){
                probleme.textContent = "Le mot de passe de confirmation n'est pas bon";
                envoi.disabled = true;
            } else {
                probleme.textContent = "";
                envoi.disabled = false;
            }
        });
    </script>
</body>
</html>