<html>
<head>
   <title>Le deuxième texte</title>
   <meta http-equiv="Content-Type" content="text/html; charset=UTF8" />
   <link rel="stylesheet" href="./style.css" />
   <link href="./dist/css/bootstrap.css" rel="stylesheet">
   <link rel="shortcut icon" href="./images/logo_le_deuxieme_texte-small.png" type="images/png" />  
</head>

<body style="background-color:white;font-size:12pt;">
<SCRIPT TYPE="text/javascript" SRC="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></SCRIPT>
<SCRIPT>
$(document).ready(function(){
   $("h2").hide();
})
</SCRIPT>

<?php
include("header.php");
?>

<!--<hr/>-->

<div  style="background-color:#F5F5F5;margin-top:20px;padding:20px;">

<div class="container">



<?php
include("parameters.php");
include("functions.php");

if (isset($_GET["id"])) {
    $id_article = intval($_GET["id"]);
} else {
    $id_article = 0;
}
//Load latest article
$sql = 'SELECT * FROM 2etexte_v1_article,2etexte_v1_utilisatrice,2etexte_v1_autrice WHERE 2etexte_v1_utilisatrice.id_utilisatrice=2etexte_v1_article.id_utilisatrice_article AND 2etexte_v1_article.id_article='.$id_article.';';
$req = mysqli_query($link, $sql)
   or die('Erreur SQL !<br>'.$sql.'<br>'.mysqli_error($link));
$data = mysqli_fetch_assoc($req);
?>



<div class="panel panel-default" style="text-align:center;padding:20px;">
  <div class="panel-body">
  <h1 class="form-signin-heading"><?php echo $data["titre_article"]; ?></h1>
  <hr/>
<?php
   displayArticle($data["id_article"],$link);
?>
  </div>
</div>

  
<?php
include("footer.php");
?> 
</body>
<script>
document.addEventListener("DOMContentLoaded", function() {
   document.querySelector("title").innerHTML = "Le deuxième texte - " + document.querySelector("h1").innerHTML;
});
</script>

</html>
