<html>
<head>
   <title>Le deuxième texte</title>
   <meta http-equiv="Content-Type" content="text/html; charset=UTF8" />
   <link rel="stylesheet" href="./style.css" />
   <link href="./dist/css/bootstrap.css" rel="stylesheet">
   <link rel="shortcut icon" href="./images/logo_le_deuxieme_texte-small.png" type="images/png" />  
</head>

<body style="background-color:white;font-size:12pt;">
<SCRIPT TYPE="text/javascript" SRC="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></SCRIPT>
<SCRIPT>
$(document).ready(function(){
   $("h2").hide();
})
</SCRIPT>

<?php
include("header.php");
?>

<!--<hr/>-->

<div  style="background-color:#F5F5F5;margin-top:20px;padding:20px;">

<div class="container">



<?php
include("parameters.php");
include("functions.php");

if (isset($_GET["id"])) {
    $id_extrait = intval($_GET["id"]);
} else {
    $id_extrait = 0;
}

$sql = 'SELECT * FROM 2etexte_v1_utilisatrice,2etexte_v1_extrait,2etexte_v1_contient_extraits,2etexte_v1_oeuvre,2etexte_v1_autrice 
WHERE 2etexte_v1_utilisatrice.id_utilisatrice=2etexte_v1_extrait.utilisatrice_extrait AND 2etexte_v1_extrait.oeuvre_extrait=2etexte_v1_oeuvre.id_oeuvre AND 2etexte_v1_oeuvre.id_autrice_oeuvre=2etexte_v1_autrice.id_autrice 
AND 2etexte_v1_extrait.id_extrait='.$id_extrait;
$req = mysqli_query($link, $sql)
or die('Erreur SQL !<br>'.$sql.'<br>'.mysqli_error($link));
$data = mysqli_fetch_assoc($req);

?>



<div class="panel panel-default" style="text-align:center;padding:20px;">
  <div class="panel-body">
  <h1 class="form-signin-heading"><?php echo $data["titre_extrait"]; ?></h1>
  <hr/>
<?php
   displayText($data,"h2",1);
?>
  </div>
</div>

  
<?php
include("footer.php");
?> 
</body>

<script>
document.addEventListener("DOMContentLoaded", function() {
   document.querySelector("title").innerHTML = "Le deuxième texte - " + document.querySelector("h1").innerHTML;
});
</script>
</html>
