<br/>
<br/>
<br/>
<a id="about"></a>

<div class="footer" style="font-family:Calibri;">
<h1>À propos...</h1>
<p>
Le projet <i>Le deuxième texte</i> est né
lors du <a href="https://forum.etalab.gouv.fr/t/a-propos-de-la-categorie-hackegalitefh/3445" target="_blank">premier
HackEgalitéFH</a>, dans l'espace de coworking <a href="http://letank.fr/">Le Tank</a>, du 3 au 5 mars 2017,<br/>
conçu par une équipe constituée de
<a href="https://twitter.com/clemencedouard">Clémence Douard</a>,
<a href="https://twitter.com/@CBrochette">Clémentine Brochier</a>,
Fil,
<a href="http://theses.fr/s112423">Anna-Livia Morand</a>
et <a href="http://igm.univ-mlv.fr/~gambette/">Philippe Gambette</a>,
avec la participation de Lauren Peuch.<br/>
<!--<a href="https://creativecommons.org/licenses/by-sa/2.0/fr/" target="_blank">CC BY-SA 2.0 FR</a>-->
</p>
<p>
Ce projet utilise <!--<a href="http://data.bnf.fr">Data BNF</a>--> 
<a href="https://www.wikidata.org">Wikidata</a> pour récupérer les années de naissance et décès des autrices et auteurs recherchés,
et pour identifier les autrices qui ont vécu à la même époque.<br/>
Notre base de données interne d'autrices utilise les dates de naissance et de décès, prénoms et noms extraits de <a href="http://data.bnf.fr">data.bnf.fr</a>,
recueillies de juin à juillet 2017.<br/>
Divers <a href="https://forum.framasoft.org/viewtopic.php?f=37&t=39834&p=295650#p295650">outils collaboratifs Framasoft</a> ont été utilisés
pour ce projet lors du HackEgalitéFH.
</p>
<p>
Contact : contact@ledeuxiemetexte.fr
</p>
<br/>
<br/>
<br/>

</div>
</div>
</div>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-296895-22', 'auto');
  ga('send', 'pageview');

</script>
